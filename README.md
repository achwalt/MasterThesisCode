# Master Thesis Code Repository 
Source code for the Master's thesis about commodity predictability using Deep Learning.  
**Get the thesis here: [Link](https://drive.google.com/file/d/1IZHRL6cJeP306eGTB3Yvw-DleNYZYWTN/view?usp=sharing)**  

This is only tested with LINUX (Ubuntu 20.04).

## Installation 
- You will need Python >= 3.8 incl. pip (in PATH) on your system. 
- If you wish to accelerate training with your GPU you will need to have NVIDIA CUDA installed as well. 
- Download this repo, or, if you have git installed: ```cmd: git clone https://gitlab.com/achwalt/MasterThesisCode.git``` 
- Ideally, start in a fresh conda environment.

```cmd:
cd path/to/dir/MasterThesisCode 
python setup.py 
```

## CAUTION 
Since this library utilizes multiple nested parallel and computationally expensive processes, tweak the following in config.py: 
### If you want to use NVIDIA CUDA:
Set 'hyperparameter_tuning/max_parallel_processes' to your number of GPUs and start from there. 
### If you use CPU-bound training: 
Set 'hyperparameter_tuning/max_parallel_processes' to your number of CPU cores divided by 2 and start from there. 

## Contents 
The important parts of the code can be found here:
- lstm.py: Implementation of the Long Short Term Memory class.
- cnn.py: Implementation of a Convolutional Neural Network class.
- linear_models.py: Helper for running linear models with mirrored functionality from Cnn and Lstm.
- data.py: The data class. This is using temporary imports due to no current access to a Bloomberg API. You will just need to look at DataModel.
- benchmarking.py: Encapsulation of the benchmarking process for given models.
- helpers.py: Reusable support functions.

Other files are mainly to support and re-use default code. 
Charts & exported tables can be found in the "Export" folder. 
Configurations can be changed in "config.yml". 
Hyperparameters are configurable in "*_hyperparams.yml".