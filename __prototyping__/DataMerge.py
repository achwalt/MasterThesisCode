#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

import pandas as pd
import re


#Functions---------------------------------------------------------------------
def rem_ve(String, listofstrings, filler = ""):
    listofstr = listofstrings
    for i in range(len(listofstr)):
        listofstr[i] = str(listofstr[i]).replace(String, filler)
    return listofstr

#Read data---------------------------------------------------------------------

def import_data():
    """Must be launched in working directory.
    Returns: Metals, Equity, Govbond, Corpbond, Macro, MSCI factors"""
    #Commodities
    metals = pd.read_csv("Data/Metals.csv", sep = ";")
    metals.iloc[1] = metals.iloc[1].ffill()
    metals.iloc[1] = rem_ve(" ", metals.iloc[1], "_")
    metals.columns = rem_ve("nan_", metals.iloc[1]+"_"+metals.iloc[4])
    metals.drop(list(range(5)), inplace = True)
    metals.index = metals[metals.columns[0]]
    metals.drop("Dates", axis = 1, inplace = True)
    metals.index = pd.to_datetime(metals.index, format = "%d.%m.%Y")
    metals = metals.astype(float)
    
    #Equity
    equity_ = pd.read_csv("Data/Equity.csv", sep = ";")
    equity_.columns = list(range(len(equity_.columns)))
    
    equity_I = equity_[list(range(9))]
    equity_II = equity_[list(range(9,11))]
    equity_III = equity_[list(range(11,len(equity_.columns)))]
    
    del equity_
    
    equity_I.columns = equity_I.iloc[4]
    equity_II.columns = equity_II.iloc[4]
    equity_III.columns = equity_III.iloc[4]
    
    equity_I.drop(list(range(6)), inplace = True)
    equity_II.drop(list(range(6)), inplace = True)
    equity_III.drop(list(range(6)), inplace = True)
    
    equity_I.index = pd.to_datetime(equity_I["Dates"], format = "%d.%m.%Y")
    equity_I.drop("Dates", axis = 1, inplace = True)
    equity_II.index = pd.to_datetime(equity_II["Dates"], format = "%d.%m.%Y")
    equity_II.drop("Dates", axis = 1, inplace = True)
    equity_III.index = pd.to_datetime(equity_III["Dates"], format = "%d.%m.%Y")
    equity_III.drop("Dates", axis = 1, inplace = True)
    
    equity = pd.DataFrame(list(range(len(metals))))
    equity.index = metals.index
    equity = equity.join(equity_I).join(equity_II).join(equity_III)
    
    del equity_I, equity_II, equity_III
    
    equity.drop(0, axis = 1, inplace = True)
    equity = equity.ffill()
    
    equity = equity.astype(float)
    
    #Government Bonds
    govbond_ = pd.read_csv("Data/GovBonds.csv", sep = ";")
    govbond_.iloc[2] = govbond_.iloc[2].ffill()
    govbond_.iloc[2] = rem_ve(" ", govbond_.iloc[2])
    govbond_.iloc[2] = rem_ve("Index", govbond_.iloc[2], "")
    govbond_.columns = rem_ve("nan_", govbond_.iloc[2]+"_"+govbond_.iloc[4])
    govbond_.drop(list(range(5)), inplace = True)
    govbond_.index = govbond_[govbond_.columns[0]]
    govbond_.drop("Dates", axis = 1, inplace = True)
    govbond_.index = pd.to_datetime(govbond_.index, format = "%d.%m.%Y")
    
    govbond = pd.DataFrame(list(range(len(metals))))
    govbond.index = metals.index
    govbond = govbond.join(govbond_)
    
    del govbond_
    
    govbond.drop(0, axis = 1, inplace = True)
    govbond = govbond.ffill()
    
    govbond = govbond.astype(float)
    
    #Corporate Bonds
    corpbond_ = pd.read_csv("Data/CorpBonds.csv", sep = ";")
    corpbond_.iloc[2] = corpbond_.iloc[2].ffill()
    corpbond_.iloc[2] = rem_ve(" ", corpbond_.iloc[2])
    corpbond_.iloc[2] = rem_ve("Index", corpbond_.iloc[2], "")
    corpbond_.columns = rem_ve("nan_", corpbond_.iloc[2]+"_"+corpbond_.iloc[4])
    corpbond_.drop(list(range(5)), inplace = True)
    corpbond_.index = corpbond_[corpbond_.columns[0]]
    corpbond_.drop("Dates", axis = 1, inplace = True)
    corpbond_.index = pd.to_datetime(corpbond_.index, format = "%d.%m.%Y")
    
    corpbond = pd.DataFrame(list(range(len(metals))))
    corpbond.index = metals.index
    corpbond = corpbond.join(corpbond_)
    
    del corpbond_
    
    corpbond.drop(0, axis = 1, inplace = True)
    corpbond = corpbond.ffill()
    
    corpbond = corpbond.astype(float)
    
    #Macro
    macro_ = pd.read_csv("Data/Macro.csv", sep = ";")
    macro_.drop(5, inplace = True)
    macro_.iloc[2] = macro_.iloc[2].ffill()
    macro_.iloc[2] = rem_ve(" ", macro_.iloc[2])
    macro_.iloc[2] = rem_ve("Index", macro_.iloc[2], "")
    macro_.columns = rem_ve("nan_", macro_.iloc[2]+"_"+macro_.iloc[4])
    macro_.drop(list(range(5)), inplace = True)
    macro_.index = macro_[macro_.columns[0]]
    macro_.drop("Dates", axis = 1, inplace = True)
    macro_.index = pd.to_datetime(macro_.index, format = "%d.%m.%Y")
    
    macro = pd.DataFrame(list(range(len(metals))))
    macro.index = metals.index
    macro = macro.join(macro_)
    
    del macro_
    
    macro.drop(0, axis = 1, inplace = True)
    macro = macro.ffill()
    
    macro = macro.astype(float)
    
    #MSCI Factors
    mscifac_ = pd.read_csv("Data/MSCI Factors.csv", sep = ";")
    mscifac_.drop(5, inplace = True)
    mscifac_.iloc[2] = mscifac_.iloc[2].ffill()
    mscifac_.iloc[2] = rem_ve(" ", mscifac_.iloc[2])
    mscifac_.iloc[2] = rem_ve("Index", mscifac_.iloc[2], "")
    mscifac_.columns = rem_ve("nan_", mscifac_.iloc[2]+"_"+mscifac_.iloc[4])
    mscifac_.drop(list(range(5)), inplace = True)
    mscifac_.index = mscifac_[mscifac_.columns[0]]
    mscifac_.drop("Dates", axis = 1, inplace = True)
    mscifac_.index = pd.to_datetime(mscifac_.index, format = "%d.%m.%Y")
    
    mscifac = pd.DataFrame(list(range(len(metals))))
    mscifac.index = metals.index
    mscifac = mscifac.join(mscifac_)
    
    del mscifac_
    
    mscifac.drop(0, axis = 1, inplace = True)
    mscifac = mscifac.ffill()
    
    mscifac = mscifac.astype(float)
    
    return metals.ffill(), equity.ffill(), govbond.ffill(), corpbond.ffill(), macro.ffill(), mscifac.ffill()