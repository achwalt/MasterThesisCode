#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

from matplotlib import pyplot
import numpy as np
from pandas import *
from DataMerge import *
import statsmodels.api as sm
import sklearn as sk
from stargazer.stargazer import Stargazer
from achwalt import *

import warnings
warnings.filterwarnings("ignore")

def equIntOLS(Y, X, PX = "PX_LAST", aggregate = "D", summariZe = True, timeShift = 0, autoReg = 0):
    _export_ = []
    for i in Xdrop(Y, "PX_LAST").columns:
        regSet = DataFrame(Y[i])*252
        regSet = regSet.join(X.shift(timeShift))
        regSet.dropna(inplace = True)
        regSet = regSet.resample(aggregate).mean()
        regSet.dropna(inplace = True)
        if autoReg > 0:
            for j in range(autoReg+1)[1:]:
                shifted = regSet.shift(j)
                shifted.columns = shifted.columns + "_AR" + str(j)
                regSet = regSet.join(shifted)
            regSet.dropna(inplace = True)
        y = regSet[i].ravel()
        x = regSet.drop(i, axis = 1)
        x = sm.add_constant(x)
        
        model = sm.OLS(y,x)
        fitted = model.fit(cov_type = "hc0")
        _export_.append(fitted)
        if summariZe == True:
            print(i)
            print(fitted.summary())
        
    return(_export_)

def expOLS(setOfModels, dependVars, filepath = "Untitled"):
    export = Stargazer(setOfModels)
    export.significant_digits(4)
    export.significance_levels([0.20, 0.1, 0.05])
    ##export.show_confidence_intervals(True)
    
    file = open(filepath + ".html", "w")
    file.write(export.render_html())
    file.write(rem_ve(str(dependVars), ["[", "]", "'"]))
    file.close()
    print("HTML exported to:", filepath + ".html")
    
    file = open(filepath + ".tex", "w")
    file.write(export.render_latex())
    file.write(rem_ve(str(dependVars), ["[", "]", "'"]))
    file.close()
    print("LaTeX exported to:", filepath + ".tex")

#Data Import-------------------------------------------------------------------

metals, equity, govbond, corpbond, macro, mscifac = import_data()

central_moments = read_csv("Tables/Central Moments/Central_Moments.csv", sep = ";", index_col = "Dates")

metalsRet = log(metals/metals.shift())

comm = list(Xdrop(metalsRet, "PX_LAST").columns)

#Equities----------------------------------------------------------------------

#Total Return Index to Returns
equity["TOT_RETURN_INDEX_GROSS_DVDS"] = log(equity["TOT_RETURN_INDEX_GROSS_DVDS"]/equity["TOT_RETURN_INDEX_GROSS_DVDS"].shift())
equity["PX_VOLUME"] = log(equity["PX_VOLUME"]/equity["PX_VOLUME"].shift())
equity["DVD_PAYOUT_RATIO"] = equity["DVD_PAYOUT_RATIO"]/100
equity["IDX_EST_DVD_CURR_YR"] = equity["IDX_EST_DVD_CURR_YR"]/100
equity["GROSS_AGGTE_DVD_YLD"] = equity["GROSS_AGGTE_DVD_YLD"]/100
equity["RETURN_COM_EQY"] = equity["RETURN_COM_EQY"]/100
equity["RETURN_ON_ASSET"] = equity["RETURN_ON_ASSET"]/100
equity["IDX_EST_PRICE_BOOK"] = equity["IDX_EST_PRICE_BOOK"]/100
equity["PX_TO_EBITDA"] = equity["PX_TO_EBITDA"]/100
#Replace Inf by NA
temp = np.where(equity == inf, np.NaN, equity.values)
equity = DataFrame(temp, index = equity.index, columns = equity.columns)

equPred = equity[equity.columns[0:-4]]
equPred.drop(["IDX_EST_DVD_CURR_YR", "IDX_EST_PRICE_BOOK"], axis = 1, inplace = True)
#equPred = equPred/100

multicoll = equPred.corr()
#Strong correlation of RoE and RoA. Removing  might enhance predictability
equPred.drop(["RETURN_COM_EQY"], axis = 1, inplace = True)

#Testing all stock-predictive variables

#Run Regessions
export_daily = equIntOLS(metalsRet, equPred, autoReg = 1)
export_weekly = equIntOLS(metalsRet, equPred, aggregate = "W", autoReg = 1)
export_monthly = equIntOLS(metalsRet, equPred, aggregate = "M", autoReg = 1)
export_quarterly = equIntOLS(metalsRet, equPred, aggregate = "Q", autoReg = 1)
export_annually = equIntOLS(metalsRet, equPred, aggregate = "Y", autoReg = 1)

export_dailyPred = equIntOLS(metalsRet, equPred, autoReg = 1, timeShift = 1)
export_weeklyPred = equIntOLS(metalsRet, equPred, aggregate = "W", autoReg = 1, timeShift = 1)
export_monthlyPred = equIntOLS(metalsRet, equPred, aggregate = "M", autoReg = 1, timeShift = 1)
export_quarterlyPred = equIntOLS(metalsRet, equPred, aggregate = "Q", autoReg = 1, timeShift = 1)
export_annuallyPred = equIntOLS(metalsRet, equPred, aggregate = "Y", autoReg = 1, timeShift = 1)

#Export to CSV
expOLS(setOfModels = export_daily, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Realtime_Daily")
expOLS(setOfModels = export_weekly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Realtime_Weekly")
expOLS(setOfModels = export_monthly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Realtime_Monthly")
expOLS(setOfModels = export_quarterly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Realtime_Quarterly")
expOLS(setOfModels = export_annually, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Realtime_Annually")

expOLS(setOfModels = export_daily, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Prediction_Daily")
expOLS(setOfModels = export_weekly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Prediction_Weekly")
expOLS(setOfModels = export_monthly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Prediction_Monthly")
expOLS(setOfModels = export_quarterly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Prediction_Quarterly")
expOLS(setOfModels = export_annually, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Equity_Prediction_Annually")

#Bonds Debt--------------------------------------------------------------------

corpbond = DataFrame(np.log(corpbond.values/corpbond.shift().values)*252, columns = corpbond.columns, index = corpbond.index)

#Corps: Returns, #Govs: Yields
bondPred = Xdrop(corpbond, "PX_LAST").join(Xdrop(np.log(1+govbond/100), "PX_LAST"))
#Check for high correlation
multicoll = bondPred.corr()
#Use shortest and longest US-Bonds only
bondPred["Corp_Default_Spread_PX_LAST"] = bondPred["SP5HYBIT_PX_LAST"] - bondPred["SP5IGBIT_PX_LAST"]
bondPred["Gov_Risk_Premium_PX_LAST"] = bondPred["USGG30YR_PX_LAST"] - bondPred["USGG3M_PX_LAST"]
bondPred["Private_Risk_Premium_PX_LAST"] = bondPred["SP5IGBIT_PX_LAST"] - bondPred["USGG30YR_PX_LAST"]

#Avoid Multicollinearity by filtering only the most relevant predictors out
bondPred_spreads = DataFrame(bondPred["SP500BDT_PX_LAST"], index = bondPred.index).join(bondPred["Corp_Default_Spread_PX_LAST"]).join(bondPred["Gov_Risk_Premium_PX_LAST"]).join(bondPred["Private_Risk_Premium_PX_LAST"])
#Run Regessions
export_daily = equIntOLS(metalsRet, bondPred_spreads, autoReg = 1)
export_weekly = equIntOLS(metalsRet, bondPred_spreads, aggregate = "W", autoReg = 1)
export_monthly = equIntOLS(metalsRet, bondPred_spreads, aggregate = "M", autoReg = 1)
export_quarterly = equIntOLS(metalsRet, bondPred_spreads, aggregate = "Q", autoReg = 1)
export_annually = equIntOLS(metalsRet, bondPred_spreads, aggregate = "Y", autoReg = 1)

export_dailyPred = equIntOLS(metalsRet, bondPred_spreads, autoReg = 1, timeShift = 1)
export_weeklyPred = equIntOLS(metalsRet, bondPred_spreads, aggregate = "W", autoReg = 1, timeShift = 1)
export_monthlyPred = equIntOLS(metalsRet, bondPred_spreads, aggregate = "M", autoReg = 1, timeShift = 1)
export_quarterlyPred = equIntOLS(metalsRet, bondPred_spreads, aggregate = "Q", autoReg = 1, timeShift = 1)
export_annuallyPred = equIntOLS(metalsRet, bondPred_spreads, aggregate = "Y", autoReg = 1, timeShift = 1)
#This leads to almost no predictability

bondPred_yields = DataFrame(bondPred["SP5HYBIT_PX_LAST"], index = bondPred.index).join(bondPred["SP5IGBIT_PX_LAST"]).join(bondPred["USGG30YR_PX_LAST"]).join(bondPred["USGG3M_PX_LAST"])
#Run Regessions
export_daily = equIntOLS(metalsRet, bondPred_yields, autoReg = 1)
export_weekly = equIntOLS(metalsRet, bondPred_yields, aggregate = "W", autoReg = 1)
export_monthly = equIntOLS(metalsRet, bondPred_yields, aggregate = "M", autoReg = 1)
export_quarterly = equIntOLS(metalsRet, bondPred_yields, aggregate = "Q", autoReg = 1)
export_annually = equIntOLS(metalsRet, bondPred_yields, aggregate = "Y", autoReg = 1)

export_dailyPred = equIntOLS(metalsRet, bondPred_yields, autoReg = 1, timeShift = 1)
export_weeklyPred = equIntOLS(metalsRet, bondPred_yields, aggregate = "W", autoReg = 1, timeShift = 1)
export_monthlyPred = equIntOLS(metalsRet, bondPred_yields, aggregate = "M", autoReg = 1, timeShift = 1)
export_quarterlyPred = equIntOLS(metalsRet, bondPred_yields, aggregate = "Q", autoReg = 1, timeShift = 1)
export_annuallyPred = equIntOLS(metalsRet, bondPred_yields, aggregate = "Y", autoReg = 1, timeShift = 1)

#Export to CSV
expOLS(setOfModels = export_daily, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Realtime_Daily")
expOLS(setOfModels = export_weekly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Realtime_Weekly")
expOLS(setOfModels = export_monthly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Realtime_Monthly")
expOLS(setOfModels = export_quarterly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Realtime_Quarterly")
expOLS(setOfModels = export_annually, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Realtime_Annually")

expOLS(setOfModels = export_daily, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Prediction_Daily")
expOLS(setOfModels = export_weekly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Prediction_Weekly")
expOLS(setOfModels = export_monthly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Prediction_Monthly")
expOLS(setOfModels = export_quarterly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Prediction_Quarterly")
expOLS(setOfModels = export_annually, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Bonds_Prediction_Annually")

#Macro Data--------------------------------------------------------------------
macroPred = macro[macro.columns[1:]]/100
export_daily = equIntOLS(metalsRet, macroPred, autoReg = 1)
export_weekly = equIntOLS(metalsRet, macroPred, aggregate = "W", autoReg = 1)
export_monthly = equIntOLS(metalsRet, macroPred, aggregate = "M", autoReg = 1)

export_dailyPred = equIntOLS(metalsRet, bondPred_yields, autoReg = 1, timeShift = 1)
export_weeklyPred = equIntOLS(metalsRet, bondPred_yields, aggregate = "W", autoReg = 1, timeShift = 1)
export_monthlyPred = equIntOLS(metalsRet, bondPred_yields, aggregate = "M", autoReg = 1, timeShift = 1)

macroPred[macro.columns[0]] = np.log(macro[macro.columns[0]]/macro[macro.columns[0]].shift())
export_quarterly = equIntOLS(metalsRet, macroPred, aggregate = "Q", autoReg = 1)
export_annually = equIntOLS(metalsRet, macroPred, aggregate = "Y", autoReg = 1)

export_quarterlyPred = equIntOLS(metalsRet, macroPred, aggregate = "Q", autoReg = 1, timeShift = 1)
export_annuallyPred = equIntOLS(metalsRet, macroPred, aggregate = "Y", autoReg = 1, timeShift = 1)

#Export to CSV
expOLS(setOfModels = export_daily, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Realtime_Daily")
expOLS(setOfModels = export_weekly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Realtime_Weekly")
expOLS(setOfModels = export_monthly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Realtime_Monthly")
expOLS(setOfModels = export_quarterly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Realtime_Quarterly")
expOLS(setOfModels = export_annually, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Realtime_Annually")

expOLS(setOfModels = export_daily, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Prediction_Daily")
expOLS(setOfModels = export_weekly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Prediction_Weekly")
expOLS(setOfModels = export_monthly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Prediction_Monthly")
expOLS(setOfModels = export_quarterly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Prediction_Quarterly")
expOLS(setOfModels = export_annually, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/Macro_Prediction_Annually")

#MSCI-Factors------------------------------------------------------------------
MSCIPred = Xdrop(DataFrame(np.log(mscifac.values/mscifac.shift().values), index = mscifac.index, columns = mscifac.columns), "PX_LAST")
MSCIPred = dropX(MSCIPred, ["MXWOQU", "MXWOSZT"])
multicoll = MSCIPred.corr()

#Run Regessions
export_daily = equIntOLS(metalsRet, MSCIPred, autoReg = 1)
export_weekly = equIntOLS(metalsRet, MSCIPred, aggregate = "W", autoReg = 1)
export_monthly = equIntOLS(metalsRet, MSCIPred, aggregate = "M", autoReg = 1)
export_quarterly = equIntOLS(metalsRet, MSCIPred, aggregate = "Q", autoReg = 1)
export_annually = equIntOLS(metalsRet, MSCIPred, aggregate = "Y", autoReg = 1)

export_dailyPred = equIntOLS(metalsRet, MSCIPred, autoReg = 1, timeShift = 1)
export_weeklyPred = equIntOLS(metalsRet, MSCIPred, aggregate = "W", autoReg = 1, timeShift = 1)
export_monthlyPred = equIntOLS(metalsRet, MSCIPred, aggregate = "M", autoReg = 1, timeShift = 1)
export_quarterlyPred = equIntOLS(metalsRet, MSCIPred, aggregate = "Q", autoReg = 1, timeShift = 1)
export_annuallyPred = equIntOLS(metalsRet, MSCIPred, aggregate = "Y", autoReg = 1, timeShift = 1)

#Export to CSV
expOLS(setOfModels = export_daily, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Realtime_Daily")
expOLS(setOfModels = export_weekly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Realtime_Weekly")
expOLS(setOfModels = export_monthly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Realtime_Monthly")
expOLS(setOfModels = export_quarterly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Realtime_Quarterly")
expOLS(setOfModels = export_annually, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Realtime_Annually")

expOLS(setOfModels = export_daily, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Prediction_Daily")
expOLS(setOfModels = export_weekly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Prediction_Weekly")
expOLS(setOfModels = export_monthly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Prediction_Monthly")
expOLS(setOfModels = export_quarterly, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Prediction_Quarterly")
expOLS(setOfModels = export_annually, dependVars = comm, filepath = "Tables/Indicator Selection/Full Data/MSCI-Factors_Prediction_Annually")