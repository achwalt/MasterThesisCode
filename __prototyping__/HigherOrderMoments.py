#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

from matplotlib import pyplot
import numpy as np
from pandas import *
from achwalt import *
from DataMerge import *
import statsmodels.api as sm
import sklearn as sk
from stargazer.stargazer import Stargazer
import scipy

import warnings
warnings.filterwarnings("ignore")


#Data Import-------------------------------------------------------------------

metals, equity, govbond, corpbond, macro, mscifac = import_data()

metals_ret = metals/metals.shift()-1


#Intraday Volatility-----------------------------------------------------------

#...as the difference of High and Low
def intradayvola(high_, low_):
    """calculates intraday volatility from two supplied dataframes of Bloomberg prices"""
    high_.columns = low_.columns
    intraday_var = (high_/low_-1)**2
    intraday_vola = (intraday_var)**0.5
    
    del high_, low_
    
    intraday_vola_cols = list(intraday_vola.columns)
    
    for i in range(len(intraday_vola.columns)):
        intraday_vola_cols[i] = intraday_vola_cols[i].replace("_PX_LOW","").replace("_PX_LAST","")
        
    intraday_vola.columns = intraday_vola_cols
    
    del intraday_vola_cols
    return intraday_vola

intraday_vola_HIGHLOW = intradayvola(Xdrop(metals, "PX_HIGH"), Xdrop(metals, "PX_LOW"))

pyplot.figure()
pyplot.title("Intraday Volatility\n(%-Spread between highest and lowest recorded price)")
pyplot.plot(intraday_vola_HIGHLOW)
pyplot.legend(intraday_vola_HIGHLOW.columns)
pyplot.savefig("Plots/Central Moments/Intraday_Volatility_SpreadHighLow", dpi = 400, quality = 100)

#...as the difference between Open and Close
intraday_vola_OPENCLOSE = intradayvola(Xdrop(metals, "PX_OPEN"), Xdrop(metals, "PX_LAST"))

pyplot.figure()
pyplot.title("Intraday Volatility\n(%-Spread between opening and closing price)")
pyplot.plot(intraday_vola_OPENCLOSE.drop("Aluminium", axis = 1)[intraday_vola_OPENCLOSE.index > to_datetime("1986-12-30")])
pyplot.legend(intraday_vola_OPENCLOSE.drop("Aluminium", axis = 1).columns)
pyplot.savefig("Plots/Central Moments/Intraday_Volatility_SpreadOpenClose", dpi = 400, quality = 100)

#Simple OLS to check if intraday vola has anything to say

for i in intraday_vola_HIGHLOW.columns:
    y = Xdrop(metals_ret, str(i)+"_PX_OPEN")
    x = sm.add_constant(intraday_vola_HIGHLOW[i]).shift()
    x = x.join(y).dropna()
    y = x[x.columns[-1]]
    x.drop(x.columns[-1], axis = 1 ,inplace = True)
    #x,y = x.to_numpy(), y.to_numpy()
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type='HC0')
    predictions = fitted.predict(x)
    print(fitted.summary())
    pyplot.figure()
    pyplot.title(i + ": Predicted by intra-day volatility")
    pyplot.scatter(y, predictions)

for i in intraday_vola_OPENCLOSE.columns:
    y = Xdrop(metals_ret, str(i)+"_PX_OPEN")
    x = sm.add_constant(intraday_vola_OPENCLOSE[i]).shift()
    x = x.join(y).dropna()
    y = x[x.columns[-1]]
    x.drop(x.columns[-1], axis = 1 ,inplace = True)
    #x,y = x.to_numpy(), y.to_numpy()
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type='HC0')
    predictions = fitted.predict(x)
    print(fitted.summary())
    pyplot.figure()
    pyplot.title(i + ": Predicted by intra-day volatility")
    pyplot.scatter(y, predictions)
    
#Absolutely no impact on next days returns
    
#Simple OLS to check if intraday vola has anything to say

for i in intraday_vola_HIGHLOW.columns:
    y = Xdrop(metals_ret, str(i)+"_PX_OPEN")
    x = sm.add_constant(Xdrop(metals_ret, str(i)+"_PX_OPEN").join(intraday_vola_HIGHLOW[i])).shift()
    x.columns = x.columns+"_x"
    x = x.join(y).dropna()
    y = x[x.columns[-1]]
    x.drop(x.columns[-1], axis = 1 ,inplace = True)
    #x,y = x.to_numpy(), y.to_numpy()
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type='HC0')
    predictions = fitted.predict(x)
    print(fitted.summary())
    pyplot.figure()
    pyplot.title(i + ": Predicted by intra-day volatility and previous return")
    pyplot.scatter(y, predictions)

for i in intraday_vola_OPENCLOSE.columns:
    y = Xdrop(metals_ret, str(i)+"_PX_OPEN")
    x = sm.add_constant(Xdrop(metals_ret, str(i)+"_PX_OPEN").join(intraday_vola_OPENCLOSE[i])).shift()
    x.columns = x.columns + "_x"
    x = x.join(y).dropna()
    y = x[x.columns[-1]]
    x.drop(x.columns[-1], axis = 1 ,inplace = True)
    #x,y = x.to_numpy(), y.to_numpy()
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type='HC0')
    predictions = fitted.predict(x)
    print(fitted.summary())
    pyplot.figure()
    pyplot.title(i + ": Predicted by intra-day volatility and previous return")
    pyplot.scatter(y, predictions)
    
#Absolutely no impact on next days returns
    

#Looking at distributions------------------------------------------------------

random.seed(1)

for i in Xdrop(metals_ret, "_PX_LAST").columns:
    comparison = Xdrop(metals_ret, i)
    comparison["Random Normal Data"] = random.normal(mean(Xdrop(metals_ret, i)), std(Xdrop(metals_ret, i)), len(Xdrop(metals_ret, i)))
    
    comparison.dropna().plot(title = "Comparison: Log Returns vs Normal Data", kind = "density").figure.savefig("Plots/Central Moments/Density_Comparison_"+str(i), dpi = 400, quality = 100)

metals_ret_norm = metals/metals.shift()

for i in Xdrop(metals_ret_norm, "_PX_LAST").columns:
    comparison = Xdrop(metals_ret_norm, i)
    comparison["Random Normal Data"] = random.normal(mean(Xdrop(metals_ret_norm, i)), std(Xdrop(metals_ret_norm, i)), len(Xdrop(metals_ret_norm, i)))
    
    comparison.dropna().plot(title = "Comparison: Arithmetic Returns vs Normal Data", kind = "density").figure.savefig("Plots/Central Moments/Density_Comparison_arithmetic_"+str(i), dpi = 400, quality = 100)

#Tendency for superslim tails
#Relevant central moments to characterize distribution
#   - Mean
#   - Standard Deviation
#   - Skewness => Probably not relevant, as very symmetric
#   - Kurtosis: Very relevant, as density is superpointy


#Taking a look at unconditional central moments
annualize = np.mean([len(metals_ret[metals_ret.index.year == i]) for i in range(1976,2019)]) 
#Normalize for ~ï261 trading days

comprehensive_table = {"Commodity": [], "Mean": [], "Variance": [], "Standard Deviation": [], "Skewness": [], "Kurtosis": []}
for i in Xdrop(metals_ret, "_PX_LAST").columns:
    comprehensive_table["Commodity"].append(i.replace("_PX_LAST","").replace("_"," "))
    comprehensive_table["Mean"].append(np.mean(Xdrop(metals_ret, "_PX_LAST")[i].dropna())*annualize)
    comprehensive_table["Variance"].append(np.var(Xdrop(metals_ret, "_PX_LAST")[i].dropna())*annualize)
    comprehensive_table["Standard Deviation"].append(np.std(Xdrop(metals_ret, "_PX_LAST")[i].dropna())*(annualize)**0.5)
    comprehensive_table["Skewness"].append(Xdrop(metals_ret, "_PX_LAST")[i].dropna().skew())
    comprehensive_table["Kurtosis"].append(Xdrop(metals_ret, "_PX_LAST")[i].dropna().kurtosis())
    
comprehensive_table = DataFrame(comprehensive_table)
comprehensive_table.index = comprehensive_table["Commodity"]
comprehensive_table.drop("Commodity", axis = 1, inplace = True)
comprehensive_table = round(comprehensive_table.T, 4)

len(metals_ret[metals_ret.index.year == 2014])

comprehensive_table.to_csv("Tables/Central Moments/Unconditional_Central_Moments.csv", sep = ";")

#Cacluating rolling central moments--------------------------------------------

daysPmonth = int(round(annualize/12,0))

#Means
central_moments = Xdrop(meanrollC(metals_ret, daysPmonth), "_rollmean")
central_moments = central_moments.join(Xdrop(meanrollC(metals_ret, daysPmonth*3), "_rollmean"))
central_moments = central_moments.join(Xdrop(meanrollC(metals_ret, daysPmonth*6), "_rollmean"))
central_moments = central_moments.join(Xdrop(meanrollC(metals_ret, daysPmonth*9), "_rollmean"))
central_moments = central_moments.join(Xdrop(meanrollC(metals_ret, daysPmonth*12), "_rollmean"))

#Standard Deviations
central_moments = central_moments.join(Xdrop(volarollC(metals_ret, daysPmonth), "_rollvol"))
central_moments = central_moments.join(Xdrop(volarollC(metals_ret, daysPmonth*3), "_rollvol"))
central_moments = central_moments.join(Xdrop(volarollC(metals_ret, daysPmonth*6), "_rollvol"))
central_moments = central_moments.join(Xdrop(volarollC(metals_ret, daysPmonth*9), "_rollvol"))
central_moments = central_moments.join(Xdrop(volarollC(metals_ret, daysPmonth*12), "_rollvol"))

#Skewness
central_moments = central_moments.join(Xdrop(skewrollC(metals_ret, daysPmonth), "_rollskew"))
central_moments = central_moments.join(Xdrop(skewrollC(metals_ret, daysPmonth*3), "_rollskew"))
central_moments = central_moments.join(Xdrop(skewrollC(metals_ret, daysPmonth*6), "_rollskew"))
central_moments = central_moments.join(Xdrop(skewrollC(metals_ret, daysPmonth*9), "_rollskew"))
central_moments = central_moments.join(Xdrop(skewrollC(metals_ret, daysPmonth*12), "_rollskew"))

#Kurtosis
central_moments = central_moments.join(Xdrop(kurtrollC(metals_ret, daysPmonth), "_rollkurt"))
central_moments = central_moments.join(Xdrop(kurtrollC(metals_ret, daysPmonth*3), "_rollkurt"))
central_moments = central_moments.join(Xdrop(kurtrollC(metals_ret, daysPmonth*6), "_rollkurt"))
central_moments = central_moments.join(Xdrop(kurtrollC(metals_ret, daysPmonth*9), "_rollkurt"))
central_moments = central_moments.join(Xdrop(kurtrollC(metals_ret, daysPmonth*12), "_rollkurt"))

central_moments.to_csv("Tables/Central Moments/Central_Moments.csv", sep = ";")

#Plot all of it:

for i in range(3,12,3):
    data = Xdrop(central_moments, "PX_LAST_rollmean_"+str(daysPmonth*i))
    data = dropX(data, "Aluminium")
    pyplot.figure()
    pyplot.title("Rolling Means of Log Returns\n Months: "+str(i))
    pyplot.plot(data)
    pyplot.legend(listrem_ve(list(data.columns), ["_PX_LAST_rollmean_"+str(daysPmonth*i)]))
    pyplot.savefig("Plots/Central Moments/Rolling Moments/rollmean_"+str(daysPmonth*i)+"_days", dpi = 400, quality = 100)
    
for i in range(3,12,3):
    data = Xdrop(central_moments, "PX_LAST_rollvol_"+str(daysPmonth*i))
    data = dropX(data, "Aluminium")
    pyplot.figure()
    pyplot.title("Rolling Volatilities of Log Returns\n Months: "+str(i))
    pyplot.plot(data)
    pyplot.legend(listrem_ve(list(data.columns), ["_PX_LAST_rollvol_"+str(daysPmonth*i)]))
    pyplot.savefig("Plots/Central Moments/Rolling Moments/rollvol_"+str(daysPmonth*i)+"_days", dpi = 400, quality = 100)
    
for i in range(3,12,3):
    data = Xdrop(central_moments, "PX_LAST_rollskew_"+str(daysPmonth*i))
    data = dropX(data, "Aluminium")
    pyplot.figure()
    pyplot.title("Rolling Skewness of Log Returns\n Months: "+str(i))
    pyplot.plot(data)
    pyplot.legend(listrem_ve(list(data.columns), ["_PX_LAST_rollskew_"+str(daysPmonth*i)]))
    pyplot.savefig("Plots/Central Moments/Rolling Moments/rollskew_"+str(daysPmonth*i)+"_days", dpi = 400, quality = 100)

for i in range(3,12,3):
    data = Xdrop(central_moments, "PX_LAST_rollkurt_"+str(daysPmonth*i))
    data = dropX(data, "Aluminium")
    pyplot.figure()
    pyplot.title("Rolling Kurtosis of Log Returns\n Months: "+str(i))
    pyplot.plot(data)
    pyplot.legend(listrem_ve(list(data.columns), ["_PX_LAST_rollkurt_"+str(daysPmonth*i)]))
    pyplot.savefig("Plots/Central Moments/Rolling Moments/rollkurt_"+str(daysPmonth*i)+"_days", dpi = 400, quality = 100)
    

#Let's check the impact on the returns

export_ = []    
for i in intraday_vola_OPENCLOSE.columns:
    y = Xdrop(metals_ret, str(i)+"_PX_LAST")
    x = sm.add_constant(Xdrop(central_moments, str(i)+"_PX_LAST").join(intraday_vola_OPENCLOSE[i])).shift()#Shift to not inlude same days return
    x.columns = x.columns + "_x"
    x = x.join(y).dropna()
    y = x[x.columns[-1]]
    x.drop(x.columns[-1], axis = 1 ,inplace = True)
    x.columns = listrem_ve(list(x.columns), [i+"_","_x"])
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type='HC0')
    predictions = fitted.predict(x)
    print(fitted.summary())
    pyplot.figure()
    pyplot.title(i + ": Predicted by all central moments")
    pyplot.scatter(y, predictions)
    export_.append(fitted)

#No predictability
    
