#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

from matplotlib import pyplot
import numpy as np
from pandas import *
from DataMerge import *

import statsmodels.api as sm

import tensorflow as tf

#Machine Learning sein Vater
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import SGD
from keras.initializers import RandomUniform, RandomNormal
from keras import backend as kerasBackend
from keras.layers import LSTM

from stargazer.stargazer import Stargazer
from achwalt import *
from tqdm import tqdm

import warnings
warnings.filterwarnings("ignore")

import os
import random

#Fix Seeds
def fixTensorFlow(seedValue):
    "Fixes a lot of random influences in Keras and its backend Tensorflow"    
    os.environ['PYTHONHASHSEED']=str(seedValue )
    random.seed(seedValue)
    np.random.seed(seedValue)
    tf.random.set_seed(seedValue)
    #(Seed for Keras Backend => TensorFlow Global)
    session_conf = tf.compat.v1.ConfigProto(intra_op_parallelism_threads = 1, inter_op_parallelism_threads = 1)
    sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=session_conf)
    tf.compat.v1.keras.backend.set_session(sess)

#Data Import-------------------------------------------------------------------

metals, equity, govbond, corpbond, macro, mscifac = import_data()

central_moments = read_csv("Tables/Central Moments/Central_Moments.csv", sep = ";", index_col = "Dates")

metalsRet = log(metals/metals.shift())
metalsRet = metalsRet[metalsRet.index >= to_datetime("2000-01-01")]
#Remove holidays: All returns will be zero on that day
metalsRet.drop_duplicates(keep = False, inplace = True)

#Assume: For daily trades you cannot get PX_LAST/PX_LAST.shift(), but only PX_LAST/PX_OPEN
metalsRetAdj = dropX(log(Xdrop(metals, "PX_LAST")/Xdrop(metals, "PX_OPEN").values), "Aluminium")
metalsRetAdj = metalsRetAdj[metalsRetAdj.index >= to_datetime("2000-01-01")]
metalsRetAdj.drop_duplicates(keep = False, inplace = True)


comm = list(Xdrop(metalsRet, "PX_LAST").columns)

#Equities----------------------------------------------------------------------

#Total Return Index to Returns
equity["TOT_RETURN_INDEX_GROSS_DVDS"] = log(equity["TOT_RETURN_INDEX_GROSS_DVDS"]/equity["TOT_RETURN_INDEX_GROSS_DVDS"].shift())
equity["PX_VOLUME"] = log(equity["PX_VOLUME"]/equity["PX_VOLUME"].shift())
equity["DVD_PAYOUT_RATIO"] = equity["DVD_PAYOUT_RATIO"]/100
equity["IDX_EST_DVD_CURR_YR"] = equity["IDX_EST_DVD_CURR_YR"]/100
equity["GROSS_AGGTE_DVD_YLD"] = equity["GROSS_AGGTE_DVD_YLD"]/100
equity["RETURN_COM_EQY"] = equity["RETURN_COM_EQY"]/100
equity["RETURN_ON_ASSET"] = equity["RETURN_ON_ASSET"]/100
equity["IDX_EST_PRICE_BOOK"] = equity["IDX_EST_PRICE_BOOK"]/100
equity["PX_TO_EBITDA"] = equity["PX_TO_EBITDA"]/100
#Replace Inf by NA
temp = np.where(equity == inf, np.NaN, equity.values)
equity = DataFrame(temp, index = equity.index, columns = equity.columns)

equPred = equity[equity.columns[0:-4]]
equPred.drop(["IDX_EST_DVD_CURR_YR", "IDX_EST_PRICE_BOOK"], axis = 1, inplace = True)
#equPred = equPred/100

multicoll = equPred.corr()
#Strong correlation of RoE and RoA. Removing  might enhance predictability
equPred.drop(["RETURN_COM_EQY"], axis = 1, inplace = True)

#Bonds-------------------------------------------------------------------------

corpbond = DataFrame(np.log(corpbond.values/corpbond.shift().values)*252, columns = corpbond.columns, index = corpbond.index)

#Corps: Returns, #Govs: Yields
bondPred = Xdrop(corpbond, "PX_LAST").join(Xdrop(np.log(1+govbond/100), "PX_LAST"))
#Check for high correlation
multicoll = bondPred.corr()
#Use shortest and longest US-Bonds only
bondPred["Corp_Default_Spread_PX_LAST"] = bondPred["SP5HYBIT_PX_LAST"] - bondPred["SP5IGBIT_PX_LAST"]
bondPred["Gov_Risk_Premium_PX_LAST"] = bondPred["USGG30YR_PX_LAST"] - bondPred["USGG3M_PX_LAST"]
bondPred["Private_Risk_Premium_PX_LAST"] = bondPred["SP5IGBIT_PX_LAST"] - bondPred["USGG30YR_PX_LAST"]

#Avoid Multicollinearity by filtering only the most relevant predictors out
bondPred_spreads = DataFrame(bondPred["SP500BDT_PX_LAST"], index = bondPred.index).join(bondPred["Corp_Default_Spread_PX_LAST"]).join(bondPred["Gov_Risk_Premium_PX_LAST"]).join(bondPred["Private_Risk_Premium_PX_LAST"])

#Macro Data--------------------------------------------------------------------

macroPred = macro[macro.columns[1:]]/100
#GDP as log growth
macroPred[macro.columns[0]] = np.log(macro[macro.columns[0]]/macro[macro.columns[0]].shift())

#MSCI Factors------------------------------------------------------------------
#Not included - Need MSCI WO outperformance
MSCIPred = Xdrop(DataFrame(np.log(mscifac.values/mscifac.shift().values), index = mscifac.index, columns = mscifac.columns), "PX_LAST")
MSCIPred = dropX(MSCIPred, ["MXWOQU", "MXWOSZT"])
multicoll = MSCIPred.corr()

#Model Tuning: Daily-----------------------------------------------------------

dailyInds = equPred.join(bondPred_spreads)
regSetPreliminary = metalsRetAdj.join(dailyInds)

class ttc(object):
    "Class to build a train- and testset incl. cross-validation. Allows for reshuffling and still seperate based on time."
    def __init__(self, df, trainSize, reshuffle = True):
        self.index = df.index
        if reshuffle == True:
            self.data = df.sample(frac = 1).copy()
        elif reshuffle == False:
            self.data = df.copy()
        else:
            raise TypeError("Input for 'reshuffle' was not understood.")    
        self.trainSize = trainSize
        self.crossSize = (1-trainSize) ** 2
        self.testSize = (1-trainSize) * trainSize
        self.columns = df.columns

    def train(self):
        return self.data[self.data.index < self.data.index[int(self.trainSize * len(self.data.index))]]
    
    def cross(self):
        mydata = self.data
        mydata = mydata[mydata.index >= self.data.index[int(self.trainSize * len(self.data.index))]]
        return mydata[mydata.index < self.data.index[int((1-self.testSize) * len(self.data.index))]]
    
    def test(self):
        return self.data[self.data.index >= self.data.index[int((1-self.testSize) * len(self.data.index))]]

#Need an own feature-scaler to be able to re-convert afterwards
class featureScaler(object):
    "Does an (x-mu)/sigma-Transformation and stores the parameters to allow reverting"
    def __init__(self, df):
        self.data = df
        self.meanParams = df.mean()
        self.stdParams = df.std()
        
    def transform(self, newData = DataFrame()):
        if len(newData.index) != 0:
            return (newData - self.meanParams)/self.stdParams
        else:
            return (self.data - self.meanParams)/self.stdParams
    
    def reverse(self, newData = DataFrame()):
        if len(newData.index) != 0:
            return newData * self.stdParams + self.meanParams
        else:
            return self.data

#Need to add lags for a Long Short-Term Memory Network model to work
def lagLeads(data, nIn = 1, nOut = 1, dropnan=True):
	kVars = 1 if type(data) == list else data.shape[1]
	df = DataFrame(data)
	cols, names = list(), list()
	#from t-n to t-1
	for i in range(nIn, 0, -1):
		cols.append(df.shift(i))
		names += [str(data.columns[j]) + "_t-" + str(i) for j in range(kVars)]
	#from t to t+n
	for i in range(0, nOut):
		cols.append(df.shift(-i))
		if i == 0:
			names += [str(data.columns[j]) for j in range(kVars)]
		else:
			names += [str(data.columns[j]) + "_t+" + str(i) for j in range(kVars)]
	#Merge it to one file
	everything = concat(cols, axis=1)
	everything.columns = names
	#Remove NA-values
	if dropnan:
		everything.dropna(inplace=True)
	return everything

def eucDist(true, pred):
    "Squared distance"
    return ((true - pred)**2)

def customLeastDist(true, pred):
    "Mean of Squared Distance and Total deviation"
    return abs((true-pred)/true) * eucDist(true, pred)

iterator = dropX(metalsRetAdj, "Steel").columns

#"Cross Validation" Tuning the model on a separate dataset
for i in iterator:
    
    X,Y = Xdrop(regSetPreliminary, dailyInds.columns).shift(), Xdrop(dropX(regSetPreliminary, "Steel"), metalsRetAdj.columns)
    
    if "Gold" in i:
        lags = 0
        fixTensorFlow(3)
        
    if "Silver" in i:
        lags = 1
        fixTensorFlow(4)
    
    elif "Palladium" in i:
        lags = 5
        fixTensorFlow(0)
        
    elif "Platin" in i:
        lags = 3
        fixTensorFlow(4)
        
    elif "Copper" in i:
        lags = 3
        fixTensorFlow(14)
    
    else:
        lags = 1
        fixTensorFlow(111)
        
    nFeatures = len(X.columns)
    X = lagLeads(X, lags, 1)
    
    X = ttc(X, 0.75, False)
    Y = ttc(Y, 0.75, False)

    
    #Filter data
    regSet = X.train().join(Y.train()[i])
    regSet.dropna(inplace = True)
    XregSet = Xdrop(regSet, X.columns)
    YregSet = Xdrop(regSet, Y.columns)
    
    #Apply feature-scaling to training data
    XScaler = featureScaler(XregSet)
    YScaler = featureScaler(YregSet)
    XtrainLM = XScaler.transform()
    Ytrain = YScaler.transform()
    
    #Reshape to 3D-Format for LSTM-model
    Xtrain = XtrainLM.values.reshape([len(XtrainLM.index), lags + 1, nFeatures]).copy()
    
    #Apply feature-scaling to cross-validation data
    XScaler = featureScaler(X.cross())
    XcrossLM = XScaler.transform()
    YScaler = featureScaler(Y.cross()[i])
    Ycross = YScaler.transform()
    
    #Reshape to 3D-Format for LSTM-model
    Xcross = XcrossLM.values.reshape((XcrossLM.shape[0], lags + 1, nFeatures))
    
    #Train the model
    epochs = 400
    model = Sequential()
    model.add(LSTM(nFeatures, input_shape=(Xtrain.shape[1], Xtrain.shape[2])))
    if "Platin" or "Palladium" or "Gold" in i:
        model.add(Dropout(0.1))
        model.add(Dense(100, activation = "tanh"))
    model.add(Dense(1, activation = "tanh"))
    #model.compile(loss='mae', optimizer='adam')
    opt = SGD(lr = 0.01, momentum = 0.1)
    model.compile(optimizer = opt, loss = eucDist, metrics = ["mean_absolute_percentage_error"])
    # fit network
    history = model.fit(Xtrain, Ytrain, epochs = epochs, batch_size = 252, validation_data=(Xcross, Ycross), verbose = 1, shuffle = False)
    
    #Linear Model as benchmark
    lmodel = sm.OLS(YregSet, sm.add_constant(XtrainLM))
    lfitted = lmodel.fit(cov_type = "hc0")
    
    #Predict future returns
    predictions = model.predict(Xcross)
    predictions = Series([k[0] for k in predictions], index = Y.cross()[i].index).rename(i)
    lpredictions = Series(lfitted.predict(sm.add_constant(XcrossLM)), index = X.cross().index).rename(i)
    
    predictions.sort_index(ascending = True, inplace = True)
    lpredictions.sort_index(ascending = True, inplace = True)
    
    predictions = YScaler.reverse(newData = predictions)
    #lpredictions = YScaler.reverse(newData = lpredictions)
    
    plotData = DataFrame(predictions).join(DataFrame(Y.cross()[i]), lsuffix = "_LSTM").join(lpredictions, rsuffix = "_LM")
    
    #R² ooS
    #print(plotData.corr()[i])
    
    #Make Plots
    pyplot.figure()
    pyplot.title(i + " (Daily): Model-Performance Cross-Validation")
    pyplot.plot(plotData.cumsum())
    pyplot.legend(["LSTM Predictions (R: " + str(round(plotData.corr()[i][0], 3)) + ")", "Actuals", "LM-Benchmark (R: " + str(round(plotData.corr()[i][2], 3)) + ")"])
    pyplot.savefig("Plots/Model Tuning/Neural Network (LSTM)/NN LSTM Daily " + i + " CrossValidation.png", dpi = 400, quality = 100)
    
    #Evaluate on test set
    
    #Apply feature-scaling to testing data
    XScaler = featureScaler(X.test())
    XtestLM = XScaler.transform()
    YScaler = featureScaler(Y.test()[i])
    Ytest = YScaler.transform()
    
    #Reshape to 3D-Format for LSTM-model
    Xtest = XtestLM.values.reshape((XtestLM.shape[0], lags + 1, nFeatures))
    
    predictions = model.predict(Xtest)
    try:
        predictions = Series([k[0] for k in predictions], index = Y.test()[i].index).rename(i)
    except ValueError:
        predictions = Series([k[0] for k in predictions], index = Y.test()[i].index[1:]).rename(i)
    lpredictions = Series(lfitted.predict(sm.add_constant(XtestLM)), index = X.test().index).rename(i)
    
    predictions.sort_index(ascending = True, inplace = True)
    lpredictions.sort_index(ascending = True, inplace = True)
    
    predictions = YScaler.reverse(newData = predictions)
    #lpredictions = YScaler.reverse(newData = lpredictions)
    
    plotData = DataFrame(predictions).join(DataFrame(Y.test()[i]), lsuffix = "_LSTM").join(lpredictions, rsuffix = "_LM")
    
    #R² ooS
    #print(plotData.corr()[i])
    
    #Make Plots
    pyplot.figure()
    pyplot.title(i + " (Daily): Model-Performance Evaluation Set")
    pyplot.plot(plotData.cumsum())
    pyplot.legend(["LSTM Predictions (R: " + str(round(plotData.corr()[i][0], 3)) + ")", "Actuals", "LM-Benchmark (R: " + str(round(plotData.corr()[i][2], 3)) + ")"])
    pyplot.savefig("Plots/Model Tuning/Neural Network (LSTM)/NN LSTM Daily " + i + " TestSet.png", dpi = 400, quality = 100)