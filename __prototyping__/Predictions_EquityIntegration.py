#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

from matplotlib import pyplot
import numpy as np
from pandas import *
from DataMerge import *
import statsmodels.api as sm
import sklearn as sk
from stargazer.stargazer import Stargazer
from achwalt import *

import warnings
warnings.filterwarnings("ignore")

#Data Import-------------------------------------------------------------------

metals, equity, govbond, corpbond, macro, mscifac = import_data()

central_moments = read_csv("Tables/Central Moments/Central_Moments.csv", sep = ";", index_col = "Dates")

metals_ret = log(metals/metals.shift())

equ_pred = equity[equity.columns[0:-4]]
equ_pred.drop(["IDX_EST_DVD_CURR_YR", "IDX_EST_PRICE_BOOK"], axis = 1, inplace = True)
equ_pred = equ_pred/100

multicoll = equ_pred.corr()
#Strong correlation of RoE and RoA. Removing  might enhance predictability
equ_pred.drop(["RETURN_COM_EQY"], axis = 1, inplace = True)
#Equities----------------------------------------------------------------------

#Testing all stock-predictive variables
export_ = []
for i in metals_ret.columns:
    reg_set = DataFrame(metals_ret[i])
    reg_set = reg_set.join(equ_pred)
    reg_set.dropna(inplace = True)
    y = reg_set[i].ravel()
    x = reg_set.drop(i, axis = 1)
    x = sm.add_constant(x)
    
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type = "hc0")
    print(i)
    print(fitted.summary())
    export_.append(fitted)



#On trailing means: 22 days
export_ = []
comm = []
for i in Xdrop(central_moments, "PX_LAST_rollmean_22").columns:
    reg_set = DataFrame(central_moments[i])
    reg_set = reg_set.join(equ_pred)
    reg_set.dropna(inplace = True)
    y = reg_set[i].ravel()
    x = reg_set.drop(i, axis = 1)*22
    x = sm.add_constant(x)
    
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type = "hc0")
    print(i)
    print(fitted.summary())
    export_.append(fitted)
    comm.append(i)
    
export = Stargazer(export_)
export.significant_digits(4)
export.show_confidence_intervals(True)

file = open("Tables/Prediction Attempts/NoShift_TrailingMeans22Days_Equities.html", "w")
file.write(export.render_html())
file.write(rem_ve(str(comm), ["[", "]", "'"]))
file.close()

file = open("Tables/Prediction Attempts/NoShift_TrailingMeans22Days_Equities.tex", "w")
file.write(export.render_latex())
file.write(rem_ve(str(comm), ["[", "]", "'"]))
file.close()

#On trailing means: 66 days
export_ = []
comm = []
for i in Xdrop(central_moments, "PX_LAST_rollmean_66").columns:
    reg_set = DataFrame(central_moments[i])
    reg_set = reg_set.join(equ_pred)
    reg_set.dropna(inplace = True)
    y = reg_set[i].ravel()
    x = reg_set.drop(i, axis = 1)*66
    x = sm.add_constant(x)
    
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type = "hc0")
    print(i)
    print(fitted.summary())
    export_.append(fitted)
    comm.append(i)
    
export = Stargazer(export_)
export.significant_digits(4)
export.show_confidence_intervals(True)

file = open("Tables/Prediction Attempts/NoShift_TrailingMeans66Days_Equities.html", "w")
file.write(export.render_html())
file.write(rem_ve(str(comm), ["[", "]", "'"]))
file.close()

file = open("Tables/Prediction Attempts/NoShift_TrailingMeans66Days_Equities.tex", "w")
file.write(export.render_latex())
file.write(rem_ve(str(comm), ["[", "]", "'"]))
file.close()

#On trailing means: 132 days
export_ = []
comm = []
for i in Xdrop(central_moments, "PX_LAST_rollmean_132").columns:
    reg_set = DataFrame(central_moments[i])
    reg_set = reg_set.join(equ_pred)
    reg_set.dropna(inplace = True)
    y = reg_set[i].ravel()
    x = reg_set.drop(i, axis = 1)*132
    x = sm.add_constant(x)
    
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type = "hc0")
    print(i)
    print(fitted.summary())
    export_.append(fitted)
    comm.append(i)
    
export = Stargazer(export_)
export.significant_digits(4)
export.show_confidence_intervals(True)

file = open("Tables/Prediction Attempts/NoShift_TrailingMeans132Days_Equities.html", "w")
file.write(export.render_html())
file.write(rem_ve(str(comm), ["[", "]", "'"]))
file.close()

file = open("Tables/Prediction Attempts/NoShift_TrailingMeans132Days_Equities.tex", "w")
file.write(export.render_latex())
file.write(rem_ve(str(comm), ["[", "]", "'"]))
file.close()

#On trailing means: 264 days
export_ = []
comm = []
for i in Xdrop(central_moments, "PX_LAST_rollmean_264").columns:
    reg_set = DataFrame(central_moments[i])
    reg_set = reg_set.join(equ_pred)
    reg_set.dropna(inplace = True)
    y = reg_set[i].ravel()
    x = reg_set.drop(i, axis = 1)*264
    x = sm.add_constant(x)
    
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type = "hc0")
    print(i)
    print(fitted.summary())
    export_.append(fitted)
    comm.append(i)
    
export = Stargazer(export_)
export.significant_digits(4)
export.show_confidence_intervals(True)

file = open("Tables/Prediction Attempts/NoShift_TrailingMeans264Days_Equities.html", "w")
file.write(export.render_html())
file.write(rem_ve(str(comm), ["[", "]", "'"]))
file.close()

file = open("Tables/Prediction Attempts/NoShift_TrailingMeans264Days_Equities.tex", "w")
file.write(export.render_latex())
file.write(rem_ve(str(comm), ["[", "]", "'"]))
file.close()


#Bonds-------------------------------------------------------------------------

