#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

from matplotlib import pyplot
import numpy as np
from pandas import *
from DataMerge import *

import statsmodels.api as sm

from stargazer.stargazer import Stargazer
from achwalt import *
from tqdm import tqdm

import warnings
warnings.filterwarnings("ignore")

import os
import random

metals, equity, govbond, corpbond, macro, mscifac = import_data()

metalsRet = log(metals/metals.shift())
metalsRet = metalsRet[metalsRet.index >= to_datetime("2000-01-01")]
#Remove holidays: All returns will be zero on that day
metalsRet.drop_duplicates(keep = False, inplace = True)

#Assume: For daily trades you cannot get PX_LAST/PX_LAST.shift(), but only PX_LAST/PX_OPEN
metalsRetAdj = dropX(log(Xdrop(metals, "PX_LAST")/Xdrop(metals, "PX_OPEN").values), "Aluminium")
metalsRetAdj = metalsRetAdj[metalsRetAdj.index >= to_datetime("2000-01-01")]
metalsRetAdj.drop_duplicates(keep = False, inplace = True)

def makeOLS(yIn,xIn, covType = "hc0"):
    y = DataFrame(yIn.copy())
    models = {"Y":[], "Model":[]}
    regSet = y.join(x)
    regSet = regSet.dropna()
    X = sm.add_constant(DataFrame(regSet[x.columns].copy()))
    Y = regSet[y.columns]
    model = sm.OLS(Y,X)
    fitted = model.fit(cov_type = covType)
    print(fitted.summary())
    return fitted
    

models = []
for i in Xdrop(metalsRet, "PX_LAST").columns:
    xIn = metalsRet[i]
    x = Xdrop(lagLeadsList(xIn, nIn = 10), "-")
    x = DataFrame(x, index = xIn.index)
    y = metalsRet[i]
    models.append(makeOLS(y, x))

def expOLS(setOfModels, dependVars, filepath = "Untitled"):
    export = Stargazer(setOfModels)
    export.significant_digits(4)
    export.significance_levels([0.20, 0.1, 0.05])
    ##export.show_confidence_intervals(True)
    
    file = open(filepath + ".html", "w")
    file.write(export.render_html())
    file.write(rem_ve(str(dependVars), ["[", "]", "'"]))
    file.close()
    print("HTML exported to:", filepath + ".html")
    
    file = open(filepath + ".tex", "w")
    file.write(export.render_latex())
    file.write(rem_ve(str(dependVars), ["[", "]", "'"]))
    file.close()
    print("LaTeX exported to:", filepath + ".tex")
    
expOLS(models, Xdrop(metalsRet, "PX_LAST").columns, filepath = "E:\Bibliotheken\Desktop\Masterarbeit\Development\Tables\Random Walk Hypothesis\RandomWalkTest")
