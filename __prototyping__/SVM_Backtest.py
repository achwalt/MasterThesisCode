#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

from matplotlib import pyplot
import numpy as np
from pandas import *
from DataMerge import *
import statsmodels.api as sm
import sklearn as sk
from sklearn.svm import SVR
from stargazer.stargazer import Stargazer
from achwalt import *
from tqdm import tqdm

import warnings
warnings.filterwarnings("ignore")

#Data Import-------------------------------------------------------------------

metals, equity, govbond, corpbond, macro, mscifac = import_data()

central_moments = read_csv("Tables/Central Moments/Central_Moments.csv", sep = ";", index_col = "Dates")

metalsRet = log(metals/metals.shift())
metalsRet = metalsRet[metalsRet.index >= to_datetime("2000-01-01")]
#Remove holidays: All returns will be zero on that day
metalsRet.drop_duplicates(keep = False, inplace = True)

#Assume: For daily trades you cannot get PX_LAST/PX_LAST.shift(), but only PX_LAST/PX_OPEN
metalsRetAdj = dropX(log(Xdrop(metals, "PX_LAST")/Xdrop(metals, "PX_OPEN").values), "Aluminium")
metalsRetAdj = metalsRetAdj[metalsRetAdj.index >= to_datetime("2000-01-01")]
metalsRetAdj.drop_duplicates(keep = False, inplace = True)


comm = list(Xdrop(metalsRet, "PX_LAST").columns)

#Equities----------------------------------------------------------------------

#Total Return Index to Returns
equity["TOT_RETURN_INDEX_GROSS_DVDS"] = log(equity["TOT_RETURN_INDEX_GROSS_DVDS"]/equity["TOT_RETURN_INDEX_GROSS_DVDS"].shift())
equity["PX_VOLUME"] = log(equity["PX_VOLUME"]/equity["PX_VOLUME"].shift())
equity["DVD_PAYOUT_RATIO"] = equity["DVD_PAYOUT_RATIO"]/100
equity["IDX_EST_DVD_CURR_YR"] = equity["IDX_EST_DVD_CURR_YR"]/100
equity["GROSS_AGGTE_DVD_YLD"] = equity["GROSS_AGGTE_DVD_YLD"]/100
equity["RETURN_COM_EQY"] = equity["RETURN_COM_EQY"]/100
equity["RETURN_ON_ASSET"] = equity["RETURN_ON_ASSET"]/100
equity["IDX_EST_PRICE_BOOK"] = equity["IDX_EST_PRICE_BOOK"]/100
equity["PX_TO_EBITDA"] = equity["PX_TO_EBITDA"]/100
#Replace Inf by NA
temp = np.where(equity == inf, np.NaN, equity.values)
equity = DataFrame(temp, index = equity.index, columns = equity.columns)

equPred = equity[equity.columns[0:-4]]
equPred.drop(["IDX_EST_DVD_CURR_YR", "IDX_EST_PRICE_BOOK"], axis = 1, inplace = True)
#equPred = equPred/100

multicoll = equPred.corr()
#Strong correlation of RoE and RoA. Removing  might enhance predictability
equPred.drop(["RETURN_COM_EQY"], axis = 1, inplace = True)

#Bonds-------------------------------------------------------------------------

corpbond = DataFrame(np.log(corpbond.values/corpbond.shift().values)*252, columns = corpbond.columns, index = corpbond.index)

#Corps: Returns, #Govs: Yields
bondPred = Xdrop(corpbond, "PX_LAST").join(Xdrop(np.log(1+govbond/100), "PX_LAST"))
#Check for high correlation
multicoll = bondPred.corr()
#Use shortest and longest US-Bonds only
bondPred["Corp_Default_Spread_PX_LAST"] = bondPred["SP5HYBIT_PX_LAST"] - bondPred["SP5IGBIT_PX_LAST"]
bondPred["Gov_Risk_Premium_PX_LAST"] = bondPred["USGG30YR_PX_LAST"] - bondPred["USGG3M_PX_LAST"]
bondPred["Private_Risk_Premium_PX_LAST"] = bondPred["SP5IGBIT_PX_LAST"] - bondPred["USGG30YR_PX_LAST"]

#Avoid Multicollinearity by filtering only the most relevant predictors out
bondPred_spreads = DataFrame(bondPred["SP500BDT_PX_LAST"], index = bondPred.index).join(bondPred["Corp_Default_Spread_PX_LAST"]).join(bondPred["Gov_Risk_Premium_PX_LAST"]).join(bondPred["Private_Risk_Premium_PX_LAST"])

#Macro Data--------------------------------------------------------------------

macroPred = macro[macro.columns[1:]]/100
#GDP as log growth
macroPred[macro.columns[0]] = np.log(macro[macro.columns[0]]/macro[macro.columns[0]].shift())

#MSCI Factors------------------------------------------------------------------
#Not included - Need MSCI WO outperformance
MSCIPred = Xdrop(DataFrame(np.log(mscifac.values/mscifac.shift().values), index = mscifac.index, columns = mscifac.columns), "PX_LAST")
MSCIPred = dropX(MSCIPred, ["MXWOQU", "MXWOSZT"])
multicoll = MSCIPred.corr()

#The first benchmark is a naive 1/N portfolio of all commodities---------------
def oneON(logReturns):
    oneOverN = logReturns
    #Assumption: If NA => Not part of the investment horizon
    #Portfolio rets cannot be calulated from continuos returns
    oneOverN = np.exp(oneOverN)-1
    oneOverN = oneOverN.mean(axis = 1)
    return oneOverN

#The second benchmark is buy & hold--------------------------------------------
def BnH(logReturns):
    buyNHold = cumprod(np.exp(logReturns)).sum(axis = 1)
    buyNHold = np.log(buyNHold/buyNHold.shift().values)
    return buyNHold

#The third benchmark is a momentum trader--------------------------------------
def repeat(x, nTimes):
    sol = []
    for i in range(nTimes):
        sol.append(x)
    return np.array(sol)

#Long and Short
def momLS(logReturns):
    momTraderLS = np.exp(logReturns)-1
    momWeightsLS = DataFrame(np.where(momTraderLS.shift() > 0, 1, np.where(momTraderLS.shift() < 0, -1, 0)))
    temp = ((repeat(momWeightsLS.abs().sum(axis = 1), len(momWeightsLS.columns)))**2)**0.5
    temp = np.where(temp == 0, 1, temp).transpose()
    momWeightsLS = momWeightsLS/temp
    momTraderLS = DataFrame((momTraderLS * momWeightsLS.values)).sum(axis = 1)
    return momTraderLS

#Poor Performance

#Go long only
def momL(logReturns):
    momTrader = np.exp(logReturns)-1
    momWeights = DataFrame(np.where(momTrader.shift() > 0, 1, 0))
    temp = ((repeat(momWeights.abs().sum(axis = 1), len(momWeights.columns)))**2)**0.5
    temp = np.where(temp == 0, 1, temp).transpose()
    momWeights = momWeights/temp
    momTrader = DataFrame((momTrader * momWeights.values)).sum(axis = 1)
    return momTrader

#The fourth Benchmark is Mean Reversal-----------------------------------------
#Long and Short
def meanRevLS(logReturns):
    mrTraderLS = np.exp(logReturns)-1
    mrWeightsLS = DataFrame(np.where(mrTraderLS.shift() > 0, -1, np.where(mrTraderLS.shift() < 0, 1, 0)))
    temp = ((repeat(mrWeightsLS.abs().sum(axis = 1), len(mrWeightsLS.columns)))**2)**0.5
    temp = np.where(temp == 0, 1, temp).transpose()
    mrWeightsLS = mrWeightsLS/temp
    mrTraderLS = DataFrame((mrTraderLS * mrWeightsLS.values)).sum(axis = 1)
    return mrTraderLS

#Long only
def meanRevL(logReturns):
    mrTrader = np.exp(logReturns)-1
    mrWeights = DataFrame(np.where(mrTrader.shift() < 0, 1, 0))
    temp = ((repeat(mrWeights.abs().sum(axis = 1), len(mrWeights.columns)))**2)**0.5
    temp = np.where(temp == 0, 1, temp).transpose()
    mrWeights = mrWeights/temp
    mrTrader = DataFrame((mrTrader * mrWeights.values)).sum(axis = 1)
    return mrTrader

#The relevant benchmarks will bei 1/N and Buy&Hold (relation might change when transaction costs are involved)
#For a comparison of model performance, an informed momentum approach will be selected
#---> Same strategy as naive momentum trader, but choice is made conditional on beliefs of future returns
#---> Future returns are obtained by a forecasting model

#BACKTEST: Support Vector Machine----------------------------------------------

#Need an own feature-scaler to be able to re-convert afterwards
class featureScaler(object):
    "Does an (x-mu)/sigma-Transformation and stores the parameters to allow reverting"
    def __init__(self, df):
        self.meanParams = df.mean()
        self.stdParams = df.std()
        
    def transform(self, df):
        return (df - self.meanParams)/self.stdParams
    
    def reverse(self, df):
        return df * self.stdParams + self.meanParams

#Misused as a VLOOKUP
def isIn(aList, aReference):
    return [True if i in aReference else False for i in aList]
    
#Forcasting model building on an expanding window regression-------------------
def SVMBTEW(y,x, freq = "Daily", tcSpread = 0, initSize = 252, leaveLowP = True, output = "full"):
    "Runs an expanding-window regression: initSize is the size of the starting-set for the data."
    modelCollection = {"Date": []}
    #Rolling Strategy
    #Symmetrical distribution to be able to aggregate by mean
    #Commented as feature scaling is conditional on information as well
    #Ytemp = (y - y.mean())/y.std()
    #Xtemp = (x - x.mean())/x.std()
    #predRets = DataFrame(columns = ("Date," + str(list(y.columns)).replace("[","").replace("]","").replace(" ","").replace("'","")).split(","))
    predRets = DataFrame(columns = y.columns)
    
    #Resample. If sum or mean only matters for the returns => Log Returns need the sum
    Ytemp = y.resample(freq[:1]).sum()
    Xtemp = x.resample(freq[:1]).mean()
    
    #Daily resampling introduces weekends => We do not need these
    #Holidays are included as well, but with zero return
    if freq[:1] == "D":
        Xtemp = Xtemp[[True if i not in [5,6] else False for i in Xtemp.index.weekday]]
        Ytemp = Ytemp[[True if i not in [5,6] else False for i in Ytemp.index.weekday]]
    
    #The regression model needs to forecast: Shift indicators one period
    Xtemp = Xtemp.shift()
    #The data of the commodity-returns as well as the indicators need to be of same length
    #Can only use entries where there is data available for the commodity -> Steel Rebar is problematic, Aluminium will need to act as cash after some period.
    Xtemp = Xtemp[Xtemp.index >= Ytemp.index[0]]
    Xtemp = Xtemp[Xtemp.index <= Ytemp.index[-1]]
    
    #Expand the window day by day, build a new forecasting regression and create equal weights conditional on expectations
    #initSize-1 as I need the latest observation for my current-day prediction and asset allocation decision
    for i in tqdm(range(initSize-1, len(Ytemp.index)-1), desc = "Support Vector Machine Backtest (Expanding Window)"):
        #The current-day info lastInfo are my indicators for today which I use for the prediction
        lastInfo = Xtemp[Xtemp.index == Xtemp.index[i+1]]
        
        #Cut X and Y according to my window-size
        Y = Ytemp[Ytemp.index <= Ytemp.index[i]]
        X = Xtemp[Xtemp.index >= Ytemp.index[0]]
        X = X[Xtemp.index <= Xtemp.index[i]]
        yCols = Y.columns
        xCols = X.columns
        #Make sure X and Y are aligned
        regSet = Y.join(X)
        #Delete empty lines if introduced (holidays)
        regSet.drop_duplicates(keep = False, inplace = True)
        regSet.dropna(how = "all")
        Y = regSet[yCols]
        X = regSet[xCols]
        #Feature Scaling for comparability of the models
        #Save parameters to rescale back later
        scX = featureScaler(X)
        X = scX.transform(X)
        #Current-day Info can only be scaled conditional on the info set
        #Therefore I use the same scaling components
        lastInfo = scX.transform(lastInfo)
        #Returns are scaled as well. Scaling parameters are stored to be able to re-convert predictions into returns
        scY = featureScaler(Y)
        Y = scY.transform(Y)
        
        X = sm.add_constant(X)
        lastInfo["const"] = 1
        lastInfo = lastInfo[X.columns]
        del xCols, yCols
        
        #Run model for each commodity        
        unscaledResults = {}
        for j in Y.columns:
            #model = SVR(kernel = "rbf", gamma = "scale", shrinking = False)
            model = SVR(kernel = "linear", gamma = "scale", shrinking = False, degree = 1, C = 1, cache_size = 57344)
            try:
                fitted = model.fit(X, Y[j])
            except ValueError:
                model = sm.OLS(Y[j],X)
                fitted = model.fit(cov_type = "hc0")
            
            #For deeper analysis all models can be stored
            if output == "inclModels":
                try:
                    modelCollection[j].append(fitted)
                    modelCollection["Date"].append(Y.index[-1])
                except KeyError:
                    modelCollection[j] = []
                    modelCollection[j].append(fitted)
                    modelCollection["Date"].append(Y.index[-1])
            
            #Predict next day returns
            unscaledResults[j] = []
            unscaledResults[j].extend(fitted.predict(lastInfo))
        #Reverse the feature-scaling to obtain returns
        unscaledResults = scY.reverse(DataFrame(unscaledResults))
        unscaledResults = DataFrame(unscaledResults)
        unscaledResults.index = [Y.index[-1]]
        #Attach predicted returns
        predRets = predRets.append(unscaledResults)
        predRets = predRets[y.columns]
    
    #Calculate the same strategies as for the naive momentum trader, but now using the forecasted returns        
    #Long Strategy
    signalsL = predRets.where(predRets < 0, 1).where(predRets >= 0, 0)
    temp = ((repeat(signalsL.abs().sum(axis = 1), len(signalsL.columns)))**2)**0.5
    temp = np.where(temp == 0, 1, temp).transpose()
    momWeights = signalsL/temp
    momReturns = exp(Ytemp[isIn(Ytemp.index, momWeights.index)])-1
    momStrategy = DataFrame((momReturns * momWeights.shift().values)).sum(axis = 1)
    
    #Long & Short
    signalsLS = predRets.where(~(predRets.isna()), 0).where(predRets <= 0, 1).where(predRets >= 0, -1)
    temp = ((repeat(signalsLS.abs().sum(axis = 1), len(signalsLS.columns)))**2)**0.5
    temp = np.where(temp == 0, 1, temp).transpose()
    momWeightsLS = signalsLS/temp
    momReturnsLS = exp(Ytemp[isIn(Ytemp.index, momWeightsLS.index)])-1
    momStrategyLS = DataFrame((momReturnsLS * momWeightsLS.shift().values)).sum(axis = 1)

    if output == "inclModels":
        return momStrategy, momStrategyLS, predRets, momReturns, momReturnsLS, momWeights, momWeightsLS, modelCollection
    elif output == "strategy":
        return momStrategy, momStrategyLS
    elif output == "weights":
        return momWeights, momWeightsLS
    elif output == "predictions":
        return predRets
    elif output == "full":
        return momStrategy, momStrategyLS, predRets, momReturns, momReturnsLS, momWeights, momWeightsLS

def plotStrat(benchmarkData, strategyDailyL, strategyDailyLS, freq = "Daily"):
    pyplot.figure()
    pyplot.plot(cumprod(1+oneON(benchmarkData)))
    pyplot.plot(cumprod(1+BnH(benchmarkData)))
    pyplot.plot(cumprod(1+momLS(benchmarkData)))
    pyplot.plot(cumprod(1+momL(benchmarkData)))
    pyplot.plot(cumprod(1+meanRevL(benchmarkData)))
    pyplot.plot(cumprod(1+meanRevLS(benchmarkData)))
    pyplot.plot(cumprod(1+strategyDailyL))
    pyplot.plot(cumprod(1+strategyDailyLS))
    pyplot.title(freq + " Results: Support Vector Machine")
    pyplot.legend(["1/N", "Buy&Hold", "Naive Momentum Long&Short", "Naive Momentum Long Only", "Mean Reversal Long&Short", "Mean Reversal Long Only", "SVM-Forecasting Strategy L&S", "SVM-Forecasting Strategy Long"], loc = "upper left")
    pyplot.savefig("Plots/Backtests/SVMStrategyComparison"+freq+" - RebalancingBenchmarkModels.png", dpi = 400, quality = 100)

#Daily
strategyDailyL, strategyDailyLS, predRetsDaily, strategyDailyInputRets, strategyDailyInputRetsLS, weightsLDaily, weightsLSDaily = SVMBTEW(metalsRetAdj, equPred.join(bondPred))
plotStrat(Xdrop(metalsRet, "PX_LAST"), strategyDailyL, strategyDailyLS)

#Weekly
strategyWeeklyL, strategyWeeklyLS, predRetsWeekly, strategyWeeklyInputRets, strategyWeeklyInputRetsLS, weightsLWeekly, weightsLSWeekly = SVMBTEW(Xdrop(metalsRet, "PX_LAST"), equPred.join(bondPred), freq = "Weekly", initSize = 52)
plotStrat(Xdrop(metalsRet, "PX_LAST").resample("Weekly"[:1]).sum(), strategyWeeklyL, strategyWeeklyLS, freq = "Weekly")

#Monthly
strategyMonthlyL, strategyMonthlyLS, predRetsMonthly, strategyMonthlyInputRets, strategyMonthlyInputRetsLS, weightsSVMonthly, weightsSVMMonthly = SVMBTEW(Xdrop(metalsRet, "PX_LAST"), equPred.join(bondPred), freq = "Monthly", initSize = 12)
plotStrat(Xdrop(metalsRet, "PX_LAST").resample("Monthly"[:1]).sum(), strategyMonthlyL, strategyMonthlyLS, freq = "Monthly")

#Quarterly
strategyQuarterlyL, strategyQuarterlyLS, predRetsQuarterly, strategyQuarterlyInputRets, strategyQuarterlyInputRetsLS, weightsLQuarterly, weightsLSQuarterly = SVMBTEW(Xdrop(metalsRet, "PX_LAST"), equPred.join(bondPred), freq = "Quarterly", initSize = 4)
plotStrat(Xdrop(metalsRet, "PX_LAST").resample("Quarterly"[:1]).sum(), strategyQuarterlyL, strategyQuarterlyLS, freq = "Quarterly")

#Yearly
strategyYearlyL, strategyYearlyLS, predRetsYearly, strategyYearlyInputRets, strategyYearlyInputRetsLS, weightsLYearly, weightsLSYearly = SVMBTEW(Xdrop(metalsRet, "PX_LAST"), equPred.join(bondPred), freq = "Yearly", initSize = 2)
plotStrat(Xdrop(metalsRet, "PX_LAST").resample("Yearly"[:1]).sum(), strategyYearlyL, strategyYearlyLS, freq = "Yearly")


#Export to CSV for a sanity-check in Excel
weightsLDaily.to_csv("Strategies/Support Vector Machine/WeightsDaily_LongOnlyExpandingWindow.csv", sep = ";")
weightsLSDaily.to_csv("Strategies/Support Vector Machine/WeightsDaily_Long&ShortExpandingWindow.csv", sep = ";")
strategyDailyInputRets.to_csv("Strategies/Support Vector Machine/ReturnsDaily_LongOnlyExpandingWindow.csv", sep = ";")
strategyDailyInputRetsLS.to_csv("Strategies/Support Vector Machine/ReturnsDaily_Long&ShortExpandingWindow.csv", sep = ";")


#Calculate the oos-R^2 as the squared empirical correlation
actuals = DataFrame((exp(Xdrop(metalsRet, "PX_LAST")[isIn(Xdrop(metalsRet, "PX_LAST").index, weightsLDaily.index)])-1))
actuals.columns = actuals.columns + "_Actuals"
RsquDaily = actuals.join(predRetsDaily)
RsquDaily = dropX(Xdrop((RsquDaily.corr()**2), "_Actuals").transpose(), "_Actuals")
RsquDaily.to_csv("Strategies/Support Vector Machine/OoSRSquared_Daily_ExpandingWindow.csv", sep = ";")
