#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

from matplotlib import pyplot
import numpy as np
from pandas import *
from achwalt import *
from DataMerge import *
import statsmodels.api as sm
import sklearn as sk
from stargazer.stargazer import Stargazer

import warnings
warnings.filterwarnings("ignore")

#Data Import-------------------------------------------------------------------

metals, equity, govbond, corpbond, macro, mscifac = import_data()

#Plotting

def plot_metal(Metal, Vol_Modifier = 1):
    pyplot.figure()
    pyplot.title(Metal.replace("_"," ")+" - Adjusted Prices")
    pyplot.plot(metals[Metal+"_PX_VOLUME"].dropna()/Vol_Modifier)
    pyplot.plot(Xdrop(metals, Metal).drop(Metal+"_PX_VOLUME", axis = 1).dropna())
    pyplot.legend([Metal+"_PX_VOLUME"] + list(Xdrop(metals, Metal).dropna().drop(Metal+"_PX_VOLUME", axis = 1).columns))
    pyplot.savefig("Plots/AdjPrices_and_Volumes_" + Metal + "CumRets", dpi = 400, quality = 100)

plot_metal("Gold", 1000)
plot_metal("Silver", 10000)
plot_metal("Platin", 50)
plot_metal("Palladium", 10)
plot_metal("Copper", 1000)
plot_metal("Steel_Rebar", 200)
plot_metal("Aluminium",1000)

metals_ret = log(metals/metals.shift()).copy()

def plot_metal_ret(Metal):
    pyplot.figure()
    pyplot.title(Metal.replace("_"," ")+" - Log Returns")
    pyplot.plot(Xdrop(metals_ret, Metal).drop(Metal+"_PX_VOLUME", axis = 1).dropna())
    pyplot.legend(list(Xdrop(metals_ret, Metal).dropna().drop(Metal+"_PX_VOLUME", axis = 1).columns))


plot_metal_ret("Gold")
plot_metal_ret("Silver")
plot_metal_ret("Platin")
plot_metal_ret("Palladium")
plot_metal_ret("Copper")
plot_metal_ret("Steel_Rebar")
plot_metal_ret("Aluminium")


#Seasonality-------------------------------------------------------------------

#Just using the total mean of all available days/year
metals_ret_season = metals_ret.copy()
#Drop DotCom and Great FC -> Asia Crisis included as US unaffected, Oil crisis was before
metals_ret_season = metals_ret_season.drop(create_interval(metals_ret_season, to_datetime("01-01-2000"), "01-01-2003")).drop(create_interval(metals_ret_season, to_datetime("01-01-2007"), "01-01-2010"))

#Remove first and last year
def drop_firstlast_year(mydata):
    ret_data = mydata.copy()
    for i in ret_data.columns:
        ret_data[i] = ret_data[i].drop(create_interval(ret_data[i], to_datetime("01-01-"+str(min(ret_data[i].index.year))), to_datetime("31-12-"+str(min(ret_data[i].index.year))))).drop(create_interval(ret_data[i], to_datetime("01-01-"+str(max(ret_data[i].index.year))), to_datetime("31-12-"+str(max(ret_data[i].index.year)))))    
    return ret_data

metals_ret_season = drop_firstlast_year(metals_ret_season)

def remove_year(mydata):
    listindex = list(mydata.copy().index)
    for i in range(len(listindex)):
        listindex[i] = to_datetime(str(2000)+"-"+str(listindex[i].month)+"-"+str(listindex[i].day), format = "%Y-%m-%d")
    return listindex

metals_ret_season.index = remove_year(metals_ret_season)
metals_ret_season = metals_ret_season.resample("D").mean()
#metals_ret_season = metals_ret_season.cumsum()

def plot_metal_season_cumret(Metal):
    pyplot.figure()
    pyplot.title(Metal.replace("_"," ")+" - Seasonality: Mean Relative Changes\n(Starting at zero)")
    pyplot.plot(Xdrop(metals_ret_season, Metal).drop(Metal+"_PX_VOLUME", axis = 1).dropna().cumsum())
    pyplot.ylabel("Average Log Cumulative Returns")
    pyplot.legend(list(Xdrop(metals_ret_season, Metal).dropna().drop(Metal+"_PX_VOLUME", axis = 1).columns))
    pyplot.savefig("Plots/Seasonality/Seasonality_" + Metal + "CumRets", dpi = 400, quality = 100)

plot_metal_season_cumret("Gold")
plot_metal_season_cumret("Silver")
plot_metal_season_cumret("Platin")
plot_metal_season_cumret("Palladium")
plot_metal_season_cumret("Copper")
plot_metal_season_cumret("Steel_Rebar")
plot_metal_season_cumret("Aluminium")

def plot_metal_season_trade(Metal):
    pyplot.figure()
    pyplot.title(Metal.replace("_"," ")+" - Seasonality: Mean Relative Changes\n(Starting from zero)")
    pyplot.plot(Xdrop(metals_ret_season, Metal+"_PX_VOLUME").dropna().cumsum())
    pyplot.legend(["Traded Volumes"])
    
plot_metal_season_trade("Gold")
plot_metal_season_trade("Silver")
plot_metal_season_trade("Platin")
plot_metal_season_trade("Palladium")
plot_metal_season_trade("Copper")
plot_metal_season_trade("Steel_Rebar")
plot_metal_season_trade("Aluminium")


#Seasonality as fluctuations around the trend----------------------------------

#Annual seasonality
metals_hp = metals.copy()

for i in metals_hp.columns:
    metals_hp[str(i+"_cycle")], metals_hp[str(i+"_trend")] = sm.tsa.filters.hpfilter(log(metals_hp[i].dropna()), 1600 * (3*30)**4)

def plot_metal_trend(Metal):
    pyplot.figure()
    pyplot.title(Metal.replace("_"," ")+" - Trend\n(Applying the Hodrick-Prescott-Filter)")
    pyplot.plot(Xdrop(Xdrop(metals_hp, "_trend"), Metal).drop(Metal+"_PX_VOLUME_trend", axis = 1).dropna())
    pyplot.ylabel("Log Prices")
    pyplot.legend(Xdrop(metals, Metal).drop(Metal+"_PX_VOLUME", axis = 1).columns)
    pyplot.savefig("Plots/Price Trend and Cycle/TrendComponent_"+Metal, dpi = 400, quality = 100)

plot_metal_trend("Gold")
plot_metal_trend("Silver")
plot_metal_trend("Platin")
plot_metal_trend("Palladium")
plot_metal_trend("Copper")
plot_metal_trend("Steel_Rebar")
plot_metal_trend("Aluminium")

def plot_metal_cycle(Metal):
    pyplot.figure()
    pyplot.title(Metal.replace("_"," ")+" - Cycle\n(Applying the Hodrick-Prescott-Filter)")
    pyplot.plot(Xdrop(Xdrop(metals_hp, "_cycle"), Metal).drop(Metal+"_PX_VOLUME_cycle", axis = 1).dropna())
    pyplot.ylabel("Absolute Deviations of Log Prices")
    pyplot.legend(Xdrop(metals, Metal).drop(Metal+"_PX_VOLUME", axis = 1).columns)
    pyplot.savefig("Plots/Price Trend and Cycle/CycleComponent_"+Metal, dpi = 400, quality = 100)

plot_metal_cycle("Gold")
plot_metal_cycle("Silver")
plot_metal_cycle("Platin")
plot_metal_cycle("Palladium")
plot_metal_cycle("Copper")
plot_metal_cycle("Steel_Rebar")
plot_metal_cycle("Aluminium")

#Capturing seasonality as the mean of the cycle
metals_season = metals_hp.copy()

metals_season.index = remove_year(metals_season)
metals_season = metals_season.resample("D").mean()
metals_season = metals_season.cumsum()

def plot_metal_season_cycle(Metal):
    pyplot.figure()
    pyplot.title(Metal.replace("_"," ")+" - Seasonality Cycle\n(Trend at zero)")
    pyplot.plot(Xdrop(Xdrop(metals_season, "_cycle"), Metal).drop(Metal+"_PX_VOLUME_cycle", axis = 1).dropna())
    pyplot.ylabel("Cumulative intra-year deviations")
    pyplot.legend(Xdrop(metals, Metal).drop(Metal+"_PX_VOLUME", axis = 1).columns)
    pyplot.savefig("Plots/Seasonality/Seasonality_" + Metal + "_Cycle", dpi = 400, quality = 100)

plot_metal_season_cycle("Gold")
plot_metal_season_cycle("Silver")
plot_metal_season_cycle("Platin")
plot_metal_season_cycle("Palladium")
plot_metal_season_cycle("Copper")
plot_metal_season_cycle("Steel_Rebar")
plot_metal_season_cycle("Aluminium")

def plot_metal_season_cyclevol(Metal):
    pyplot.figure()
    pyplot.title(Metal.replace("_"," ")+" - Seasonality: Traded Volumes\n(Log Data, Trend at zero)")
    pyplot.plot(Xdrop(metals_season, Metal+"_PX_VOLUME_cycle").dropna())
    pyplot.legend(Xdrop(metals, Metal).columns)
    
plot_metal_season_cyclevol("Gold")
plot_metal_season_cyclevol("Silver")
plot_metal_season_cyclevol("Platin")
plot_metal_season_cyclevol("Palladium")
plot_metal_season_cyclevol("Copper")
plot_metal_season_cyclevol("Steel_Rebar")
plot_metal_season_cyclevol("Aluminium")


reg_set = DataFrame(Xdrop(metals_ret, "_PX_LAST")).copy().dropna(how = "all")
reg_set["Season"] = remove_year(reg_set)
metals_ret_season["Season"] = metals_ret_season.index
reg_set_index = reg_set.index
reg_set = reg_set.merge(metals_ret_season, on = "Season", how = "left")
reg_set.index = reg_set_index
del reg_set_index
reg_set = dropX(Xdrop(reg_set, "_PX_LAST"), ["Aluminium", "Rebar"])
reg_set.columns = [i.replace("_x", "").replace("_y","_Season") for i in reg_set.columns]

#Correlations------------------------------------------------------------------

seasonality_correlations = reg_set.corr()
seasonality_correlations.to_csv("Tables/Seasonality/Season_Correlations.csv", sep = ";")

#Regression Analysis-----------------------------------------------------------

#x and y are switched in column naming
season_regressions = {"Commodity": [], "Model": []}
iterator = 0
for i in Xdrop(reg_set, "_Season"):

    #1. Add intercept
    inner_reg = DataFrame({str(reg_set.columns[iterator + 4]).replace("_PX_LAST",""):reg_set[reg_set.columns[iterator + 4]].to_numpy().astype(float32), "y":reg_set[reg_set.columns[iterator]].to_numpy().astype(float32)})
    inner_reg.dropna(inplace = True)
    
    x = sm.add_constant(inner_reg[str(reg_set.columns[iterator + 4]).replace("_PX_LAST","")])
    y = inner_reg["y"]
    del inner_reg
    
    #2. Build model
    model = sm.OLS(y,x)
    results = model.fit(cov_type='HC0')#Robust White COV estimator
    season_regressions["Commodity"].append(i.replace("_y", ""))
    season_regressions["Model"].append(results)
    print(i)
    print(results.summary())
    del x,y,model,results
    iterator += 1
del iterator

#Export regressions
export = Stargazer(season_regressions["Model"])
export.significant_digits(4)
export.show_confidence_intervals(True)

#Export to file

file = open("Tables/Seasonality/Season_OLS_Reg.html", "w")
file.write(export.render_html())
file.close()

file = open("Tables/Seasonality/Season_OLS_Reg.tex", "w")
file.write(export.render_latex())
file.close()

#Strong significance - Useful!
Xdrop(reg_set, "_Season").to_csv("Tables/Seasonality/Seasonality_Factor.csv", sep = ";")

#Predicting the cycle only-----------------------------------------------------

metals_cycle = Xdrop(metals_hp, "PX_LAST_cycle").copy().dropna(how = "all")
metals_cycle["Season"] = remove_year(metals_cycle)
metals_cycindex = metals_cycle.index
season = Xdrop(metals_season, "PX_LAST_cycle")
season["Season"] = season.index
metals_cycle = metals_cycle.merge(season, on = "Season", how = "left")
metals_cycle.index = metals_cycindex
metals_cycle = dropX(metals_cycle, ["Aluminium", "Rebar"])
metals_cycle.columns = [i.replace("_x", "").replace("_y","_Season") for i in metals_cycle.columns]
metals_cycle.drop("Season", axis = 1, inplace = True)

cycle_correlations = metals_cycle.corr()

season_cycle_regressions = {"Commodity": [], "Model": []}
iterator = 0
for i in Xdrop(metals_cycle, "_Season"):

    #1. Add intercept
    inner_reg = DataFrame({str(metals_cycle.columns[iterator + 4]).replace("_PX_LAST",""):metals_cycle[metals_cycle.columns[iterator + 4]].to_numpy().astype(float32), "y":metals_cycle[metals_cycle.columns[iterator]].to_numpy().astype(float32)})
    inner_reg.dropna(inplace = True)
    
    x = sm.add_constant(inner_reg[str(metals_cycle.columns[iterator + 4]).replace("_PX_LAST","")])
    y = inner_reg["y"]
    del inner_reg
    
    #2. Build model
    model = sm.OLS(y,x)
    results = model.fit(cov_type='HC0')#Robust White COV estimator
    season_cycle_regressions["Commodity"].append(i.replace("_y", ""))
    season_cycle_regressions["Model"].append(results)
    print(i)
    print(results.summary())
    del x,y,model,results
    iterator += 1
del iterator

#Export to file

export = Stargazer(season_cycle_regressions["Model"])
export.significant_digits(4)
export.show_confidence_intervals(True)

file = open("Tables/Seasonality/Season_cycle_OLS_Reg.html", "w")
file.write(export.render_html())
file.close()

file = open("Tables/Seasonality/Season_cycle_OLS_Reg.tex", "w")
file.write(export.render_latex())
file.close()

#Export cyclical factor?
seasonality_cyc = DataFrame(metals_cycle.index)
seasonality_cyc.index = metals_cycle.index
seasonality_cyc["Season"] = remove_year(seasonality_cyc)
seasonality_cyc.drop("Dates", axis = 1, inplace = True)
seasonality_cyc = seasonality_cyc.merge(season, on = "Season", how = "left")
seasonality_cyc.index = metals_cycle.index
seasonality_cyc.drop("Season", axis = 1, inplace = True)

y_set = dropX(Xdrop(metals_cycle, "_PX_LAST_cycle"), ["_Season", "Aluminium", "Steel"])
seasonality_cyc = dropX(seasonality_cyc, ["Aluminium", "Steel"])
reg_set = DataFrame({})
for i in y_set.columns:
    reg_set["y"] = y_set[i]
    reg_set["x"] = seasonality_cyc[i]
    reg_set.dropna(inplace = True)
    print(i, reg_set.corr())
    x = sm.add_constant(reg_set["x"])
    y = reg_set["y"].ravel()
    model = sm.OLS(y,x)
    fitted = model.fit(cov_type = "hc0")
    print(fitted.summary())
    