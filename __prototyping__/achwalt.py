#Author------------------------------------------------------------------------
#Benjamin Albrechts

#Machine Learning: Important functions to be reused throughout the project


#Libraries---------------------------------------------------------------------
from numpy import *
from pandas import *
from matplotlib import pyplot
import statsmodels.api as sm


#Useful functions--------------------------------------------------------------
def Xdrop(mydata, leave):
    "Drops every column from dataframe except what is supplied in list leave."
    try:
        to_drop = []
        for i in mydata.columns:
            if leave not in i:
                to_drop.append(i)
        return mydata.drop(to_drop, axis = 1)
    except TypeError:
        to_drop = []
        for k in leave:
            for i in mydata.columns:
                if k not in i:
                    to_drop.append(i)
        for k in leave:
            j = 0
            while j < len(to_drop):
                if k in to_drop[j]:
                    to_drop.remove(to_drop[j])
                else:
                    j += 1
        return mydata.drop(to_drop, axis = 1)
    
def dropX(mydata, kill):
    "Drops columns according to unique identifiers."
    try:
        to_drop = []
        for i in mydata.columns:
            if kill in i:
                to_drop.append(i)
        return mydata.drop(to_drop, axis = 1)
    except TypeError:
        to_drop = []
        for k in kill:
            for i in mydata.columns:
                if k in i:
                    to_drop.append(i)
        return mydata.drop(to_drop, axis = 1)
    

def search_(mydata, term, column_names = 0):
    "Search for inconvertible data in mydata (dataframe with rows and columns), term is a string, column_names allows restriction of columns."
    if str(column_names) == "0":
        column_names = list(mydata.columns)
    counter = 0
    for i in column_names:
        for j in range(len(mydata[i])):
            if str(term) in str(mydata[i][j]):
                counter += 1
    return counter


def search_inconvertible(mydata, column_names = 0):
    "Search for missing data in mydata (dataframe with rows and columns), term is a string, column_names allows restriction of columns."
    if str(column_names) == "0":
        column_names = list(mydata.columns)
    counter = 0
    faulty_ = {"column":[], "row":[], "value":[]}
    for i in column_names:
        for j in range(len(mydata[i])):
            try:
                float(mydata[i][j])
            except TypeError:
                counter += 1
                faulty_["column"].append(i)
                faulty_["row"].append(j)
                faulty_["value"].append(mydata[i][j])
    print("Inconvertible entries found: ", counter)
    return faulty_


def calcret(prices, logarithmic = False):
    "Calculate returns of a given list of prices. Logarithmic indicates log returns instead of default normal returns."
    returns = [NaN]
    if logarithmic == False:
        for i in range(len(prices)):
            if i > 0:
                returns.append(prices[i]/prices[i-1]-1)
    else:
        for i in range(len(prices)):
            if i > 0:
                returns.append(log(prices[i]/prices[i-1]))
    return returns


def force_convert(mydata, index_ = 0, replace = False, by100 = False):
    "Forces conversion of every element in dataframe 'mydata' to a float. Use carefully!"
    if str(index_) == "0":
        index_ = mydata.columns
    for i in index_:
        appendix = []
        for j in range(len(mydata[i])):
            if replace == True:
                mydata[i][j] = float(mydata[i][j])
                if by100 == "div":
                    mydata[i][j] /= 100
                elif by100 == "mul":
                    mydata[i][j] /= 100
            else:
                if by100 == "div":
                    appendix.append(float(mydata[i][j])/100)
                elif by100 == "mul":
                    appendix.append(float(mydata[i][j])*100)
                else:
                    appendix.append(float(mydata[i][j]))
                    
        if replace == False:
            mydata[str(i)+"_ret"] = appendix


def cumret(returns, logarithmic = False):
    "Cumulates returns - returns is a list, logarithmic indicates if normal or log returns."
    cumreturn = 0
    cumreturns = []
    if logarithmic == False:
        for i in range(len(returns)):
            cumreturn = (1 + cumreturn)*(1 + returns[i])-1 #geometric linking of total returns
            cumreturns.append(cumreturn)
        return cumreturns
    else:
        for i in range(len(returns)):
            cumreturn = cumreturn + returns[i]
            cumreturns.append(cumreturn)
        return cumreturns
    

def hodrickprescott(mydata_input, relevant_columns = 0, lambda_ = 1600 * (3*30)**4):
    "Runs the Hodrick-Prescott-Filter on a supplied log time series. Mydata is the relevant dataset, relevant_columns is a list of unique identifiers, lambda is trade-off between smooth or accurate trend (Ravn/Uhlig)."
    mydata = mydata_input
    if str(relevant_columns) == "0":
        relevant_columns = mydata.columns
    for i in relevant_columns:
        mydata[str(i+"_cycle")], mydata[str(i+"_trend")] = sm.tsa.filters.hpfilter(1+mydata[str(i+"_cumret")], lambda_)
    return mydata
    
        
def geomean(alist):
    "Calculates the geometric mean of a list of total returns."
    return cumprod(DataFrame(alist)+1).iloc[-1]**(1/len(alist))-1


def meanrollC(input_data, delay, index_ = 0):
    "Calculates the moving average of a time series cycle component. Fails if unique identifier '_cycle' is not found! mydata is pandas dataframe, index_ is the relevant columns, delay is integer length of the measurement period."
    mydata = input_data.copy()
    if str(index_) == str(0):
        index_ = mydata.columns
    for i in index_:
        mydata[str(i + "_rollmean_" + str(delay))] = mydata[i].rolling(delay).mean()
    return mydata
        

def volarollC(input_data, delay, index_ = 0):
    "Calculates the moving volatility of a time series. mydata is pandas dataframe, index_ is the relevant columns, delay is integer length of the measurement period."
    mydata = input_data.copy()
    if str(index_) == str(0):
        index_ = mydata.columns
    for i in index_:
        mydata[str(i + "_rollvol_" + str(delay))] = mydata[i].rolling(delay).std()
    return mydata


def skewrollC(input_data, delay, index_ = 0):
    "Calculates the moving skewness of a time series. mydata is pandas dataframe, index_ is the relevant columns, delay is integer length of the measurement period."
    mydata = input_data.copy()
    if str(index_) == str(0):
        index_ = mydata.columns
    for i in index_:
        mydata[str(i + "_rollskew_" + str(delay))] = mydata[i].rolling(delay).skew()
    return mydata


def kurtrollC(input_data, delay, index_ = 0):
    "Calculates the moving skewness of a time series. mydata is pandas dataframe, index_ is the relevant columns, delay is integer length of the measurement period."
    mydata = input_data.copy()
    if str(index_) == str(0):
        index_ = mydata.columns
    for i in index_:
        mydata[str(i + "_rollkurt_" + str(delay))] = mydata[i].rolling(delay).kurt()
    return mydata
        
        
def plot_ts(mydata, columns_, logarithmic = False, atitle = "", y_label = False, identifier = "", df = True, xaxis = False, legendloc = 0, export_path = False):
    "Plots a time-series. mydata can be list or dataframe. logarithmic is for logarithmic returns => Deduct 1 if any other data is to be portrayed. identifier (string) allows filtering of data according to unique identifier string in column header."
    pyplot.figure()
    pyplot.title(atitle)
    pyplot.xlabel("Time")
    if y_label != False:
        pyplot.ylabel(y_label)
    if logarithmic != False:
        if df == True:
            for i in mydata.columns:
                if identifier in i:
                    pyplot.plot(log(1+mydata[i]))
        else:
            pyplot.plot(log(1+DataFrame(mydata)))
    else:
        if df == True:
            for i in mydata.columns:
                if identifier in i:
                    pyplot.plot(mydata[i])
        else:
            pyplot.plot(DataFrame(mydata, index = xaxis))
    pyplot.legend(columns_, loc = legendloc)
    if export_path != False:
        pyplot.savefig(export_path, dpi = 400, quality = 100)
        

def replbyret(mydata, index_ = [0], logarithmic = False, replace = False):
    "gets total returns for a supplied dataset"
    if index_[0] == 0:
        index_ = mydata.columns
    if replace == False:
        for i in index_:
            mydata[str(i+"_ret")] = calcret(mydata[i], logarithmic)
    else:
        for i in index_:
            RAMeater = mydata[i]
            mydata[i] = calcret(RAMeater, logarithmic)
            

def import_datastream(filepath, asset_class = 0, typus = 0, basis = 0, convert_date = True, compute_returns = False, logarithmic = False, replace_by_returns = False, by100 = False, formatting = "%d.%m.%Y"):
    "Imports csv from datastream. Uses COMMA as seperator. No spaces allowed. convert_date only if identifier is unique! Computes returns on the whole dataset if set to True. by100 either 'div' or 'mul'"
    mydata = read_csv(filepath)

    #Easy mapping of indices and names, remove index row
    mydata_mapping = DataFrame(mydata.iloc[[0]])
    trashcan = mydata.values[0].tolist()
    mydata.drop(labels = [0], axis = 0, inplace = True)
    
    #Rename column
    trashcan[0] = "Date"
    mydata.columns = trashcan
    mydata.index = mydata["Date"]
    mydata.drop(mydata.columns[0], axis = 1, inplace = True)
    
    if convert_date == True:    
        #Assign timestamp
        mydata.index = to_datetime(mydata.index, format = formatting)
        mydata_prices = mydata
    
    #convert to numbers
    force_convert(mydata, replace = True, by100 = by100)
    
    if compute_returns == True:
        #Only keep total returns for the analysis
        replbyret(mydata, mydata.columns, logarithmic, replace_by_returns)
    
    indices = []    
    for i in mydata_mapping:
        indices.append(mydata_mapping[i][0])   
    mapping = transpose(DataFrame({"index": indices, "description":mydata_mapping.columns, "class": [str(asset_class)] * len(mydata_mapping.columns), "type": [str(typus)] * len(mydata_mapping.columns), "basis": [str(basis)] * len(mydata_mapping.columns)}))
    mapping.columns = indices    
    mapping.drop([mapping.columns[0]], axis = 1, inplace = True)
    return mydata, mapping


def read_database(filepath):
    "Reads a time-series dataset wih first column as unique dates."
    mydata = read_csv(filepath)
    
    mydata.index = to_datetime(mydata[mydata.columns[0]])
    mydata.drop([mydata.columns[0]], axis = 1, inplace = True)
    return mydata


def read_mapping(filepath):
    "Reads the mapping table with first columns corresponding to the index."
    mydata = read_csv(filepath)
    mydata.index = mydata[mydata.columns[0]]
    mydata.drop([mydata.columns[0]], axis = 1, inplace = True)
    return mydata


def class_to_index(mydata, mapping, asset_class):
    "Create an index of all titles belonging to a given asset class."
    indices = []
    for i in mydata.columns:
        for j in mapping.columns:
            if str(i) == str(j):
                if mapping[j]["class"] == asset_class:
                    indices.append(mapping[j]["index"])
    return indices


def mapind(terminus, mapping):
    "Provides information of the nature for a given datastream index."
    counter = 0
    for i in mapping.columns:
        if i == terminus:
            counter += 1
            print("---------------------------------------------------\n"+str(terminus)+":",mapping[i]["description"],"\nAsset Class:", mapping[i]["class"],"\nVariable Type:", mapping[i]["type"],"\nOriginates from:", mapping[i]["basis"],"\n----------------------------------------------------")
    if counter == 0:
        print(terminus, "not found.")

        
def convert_to_logret(mydata, index_ = 0):
    "Converts a dataframe of discrete returns to log returns."
    if str(index_) == 0:
        actual_data = mydata
    else:
        actual_data = Xdrop(mydata, index_)
    return log(1+actual_data)


def convert_to_normalret(mydata, index = 0):
    "Converts a dataframe of log returns to discrete returns."
    if str(index_) == 0:
        actual_data = mydata
    else:
        actual_data = Xdrop(mydata, index_)
    return e**(actual_data)-1


def create_interval(mydata, from_date, to_date):
    """Takes timestamp and returns a list of index items within a given interval."""
    interval = []
    for i in mydata.index:
        if to_datetime(from_date) < to_datetime(i) < to_datetime(to_date):
            interval.append(i)
    return interval


def rem_ve(string, alist):
    "Removes everything contained in alist from string."
    for i in range(len(alist)):
        string = string.replace(alist[i],"")
    return string

def listrem_ve(remove_from_list, alist):
    """Removes everything contained in alist from a list of strings.
    remove_from_list: List to be worked on.
    alist: List of string patterns to remove"""
    removefrom = remove_from_list
    for i in range(len(removefrom)):
        removefrom[i] = rem_ve(removefrom[i], alist)
    return removefrom

class ttc(object):
    "Class to build a train- and testset incl. cross-validation. Allows for reshuffling and still seperate based on time."
    def __init__(self, df, trainSize, reshuffle = True):
        self.index = df.index
        if reshuffle == True:
            self.data = df.sample(frac = 1).copy()
        elif reshuffle == False:
            self.data = df.copy()
        else:
            raise TypeError("Input for 'reshuffle' was not understood.")    
        self.trainSize = trainSize
        self.crossSize = (1-trainSize) ** 2
        self.testSize = (1-trainSize) * trainSize
        self.columns = df.columns
    def train(self):
        return self.data[self.data.index < self.data.index[int(self.trainSize * len(self.data.index))]]   
    def cross(self):
        mydata = self.data
        mydata = mydata[mydata.index >= self.data.index[int(self.trainSize * len(self.data.index))]]
        return mydata[mydata.index < self.data.index[int((1-self.testSize) * len(self.data.index))]]
    def test(self):
        return self.data[self.data.index >= self.data.index[int((1-self.testSize) * len(self.data.index))]]
    
class featureScaler(object):
    "Does an (x-mu)/sigma-Transformation and stores the parameters to allow reverting"
    def __init__(self, df):
        self.data = df
        self.meanParams = df.mean()
        self.stdParams = df.std()
        
    def transform(self, newData = DataFrame()):
        if len(newData.index) != 0:
            return (newData - self.meanParams)/self.stdParams
        else:
            return (self.data - self.meanParams)/self.stdParams
    
    def reverse(self, newData = DataFrame()):
        if len(newData.index) != 0:
            return newData * self.stdParams + self.meanParams
        else:
            return self.data

#Bug in Spyder makes changes to this function break its syntax
def lagLeads(data, nIn = 1, nOut = 1, dropnan=True):    
	kVars = 1 if type(data) == list else data.shape[1]
	df = DataFrame(data)
	cols, names = list(), list()
	#from t-n to t-1
	for i in range(nIn, 0, -1):
		cols.append(df.shift(i))
		names += [str(data.columns[j]) + "_t-" + str(i) for j in range(kVars)]
	#from t to t+n
	for i in range(0, nOut):
		cols.append(df.shift(-i))
		if i == 0:
			names += [str(data.columns[j]) for j in range(kVars)]
		else:
			names += [str(data.columns[j]) + "_t+" + str(i) for j in range(kVars)]
	#Merge it to one file
	everything = concat(cols, axis=1)
	everything.columns = names
	#Remove NA-values
	if dropnan:
		everything.dropna(inplace=True)
	return everything

def lagLeadsList(data, nIn = 1, nOut = 0, dropnan = True):
    cols = DataFrame(data.copy())
    if len(cols.columns) != 1:
        raise TypeError("This function is for single-column dataframes only. Use 'lagLeads' instead.")
    else:
        cols.columns = ["t"]
        for i in range(nIn, 0, -1):
            cols = cols.join(cols["t"].shift(i), rsuffix = "-" + str(i))
        for i in range(0, nOut):
            cols = cols.join(cols["t"].shift(-(1+i)), rsuffix = "+" + str(1+i))
        return cols