#Meta--------------------------------------------------------------------------

#Benjamin Albrechts
#Purpose: Benchmarking of Machine Learning Models vs OLS

#Import------------------------------------------------------------------------

from data import Data, DataModel, ContainerPredictedObserved

from linear_models import LinearModels
from lstm import Lstm
from cnn import Cnn

from plots import Plots

from helpers import translate_commodity, translate_resampling, latex_table

import multiprocessing as mp
import numpy as np
import pandas as pd
import os

class Benchmarking:
    
    def __init__(self, data = None, data_model = None):
        self.perf_lstm = ContainerPredictedObserved()
        self.perf_cnn = ContainerPredictedObserved()
        self.perf_lm = ContainerPredictedObserved()
        
        self.data = data if data else Data()
        self.data_model = data_model if data_model else DataModel(self.data)
        self.config = self.data.config
        
        self.lstm = Lstm(data = self.data)
        self.cnn = Cnn(data = self.data)
        self.lm = LinearModels(data = self.data)
        
        self.plots = Plots()
        self.results_collection = mp.Manager().list()
    
    def process_wrapper(self, function, args, queue, key):
        result = function(*args)
        queue[key] = result
        
    def benchmark(self, frequency, data_model, custom_specification = {"lstm": {}, "cnn": {}, "lm": {}}):
        assert sorted(custom_specification.keys()) == ['cnn', 'lm', 'lstm'], "Keys for custom_specification are ['cnn', 'lm', 'lstm']"
        dataset = self.data_model.map[data_model] if type(data_model) == str else data_model
        dataset = dataset(frequency = frequency, shift = 1)
        procs = [] 
        queue = mp.Manager().dict()
        
        p = mp.Process(target = self.process_wrapper, args = (self.lstm.run, (frequency, data_model, [], "standard", True, False, custom_specification["lstm"]), queue, "lstm"))
        p.start()
        procs.append(p)
        
        p = mp.Process(target = self.process_wrapper, args = (self.cnn.run, (frequency, data_model, [], "standard", True, False, custom_specification["cnn"]), queue, "cnn"))
        p.start()
        procs.append(p)
        
        p = mp.Process(target = self.process_wrapper, args = (self.lm.run, (frequency, data_model, [], "standard", True, False, custom_specification["lm"]), queue, "lm"))
        p.start()
        procs.append(p)
        
        for p in procs:
            p.join()
        for p in procs: #Shared Memory Fix
            p.close()
            procs.remove(p)
        
        self.perf_lstm = queue["lstm"]
        self.perf_cnn = queue["cnn"]
        self.perf_lm = queue["lm"]
        
        return self.perf_lstm, self.perf_cnn, self.perf_lm
    
    def add_rows_to_collection(self, container_object, model_name, frequency, data_model):
        for key in container_object.data.keys():
            row = {"Model": model_name, "Frequency": translate_resampling(frequency), "Data Model": data_model, "Commodity": translate_commodity(key)}
            row["Performance"] = np.corrcoef(container_object.data[key]["observed"], container_object.data[key]["predicted"])[0][1] * 100
            self.results_collection.append(row)
    
    def mkdir(self, fpath):
        folder_dir = os.path.split(fpath)[0]
        try: os.makedirs(folder_dir)
        except: pass
        return fpath
    
    def export_results(self, fname):
        df = pd.DataFrame([i for i in self.results_collection]).sort_values(by = ["Model", "Frequency", "Data Model", "Commodity"])
        df_split = {"LSTM": df.loc(0)[df.loc(1)["Model"] == "LSTM"], "CNN": df[df.loc(1)["Model"] == "CNN"], "LM": df[df.loc(1)["Model"] == "LM"]}
        for key in df_split:
            df_split[key][key] = df_split[key].loc(1)["Performance"]
            df_split[key] = df_split[key].drop(["Performance"], axis = 1)
        df = df_split["LSTM"].reset_index(drop = True)
        df["CNN"] = df_split["CNN"]["CNN"].rename("CNN").reset_index(drop = True)
        df["LM"] = df_split["LM"]["LM"].rename("LM").reset_index(drop = True)
        df = df.fillna(0).drop(["Model"], axis = 1)
        latex = latex_table(df.to_latex(index = False), "Benchmarking Results")
        fname = os.path.splitext(fname)[0]
        export_path = self.mkdir(os.path.join(self.config["export_folder"]["main"], self.config["export_folder"]["tables"]["main"], self.config["export_folder"]["tables"]["latex"], fname + ".tex"))
        with open(export_path, "w", encoding = "utf-8-sig") as f:
            f.write(latex)
        export_path = self.mkdir(os.path.join(self.config["export_folder"]["main"], self.config["export_folder"]["tables"]["main"], self.config["export_folder"]["tables"]["html"], fname + ".html"))
        df.to_html(export_path, index = False)
        export_path = self.mkdir(os.path.join(self.config["export_folder"]["main"], self.config["export_folder"]["tables"]["main"], self.config["export_folder"]["tables"]["csv"], fname + ".csv"))
        df.to_csv(export_path, index = False)
        return df
    
    def run(self, frequency, data_model, custom_specification = {"lstm": {}, "cnn": {}, "lm": {}}):
        self.benchmark(frequency, data_model, custom_specification)
        self.plots.predictions_benchmark(self.perf_lm, self.perf_lstm, title = f"LSTM vs. LM | {data_model} | {frequency}", x_label = "OLS", y_label = "LSTM", legend_loc = 0, fname_prefix = f"LSTM_benchmark_{frequency}_{data_model}", topfolder = "Benchmarking", new_fig = True)
        self.plots.predictions_benchmark(self.perf_lm, self.perf_cnn, title = f"CNN vs. LM | {data_model} | {frequency}", x_label = "OLS", y_label = "CNN", legend_loc = 0, fname_prefix = f"CNN_benchmark_{frequency}_{data_model}", topfolder = "Benchmarking", new_fig = True)
        self.add_rows_to_collection(self.perf_lstm, "LSTM", frequency, data_model)
        self.add_rows_to_collection(self.perf_cnn, "CNN", frequency, data_model)
        self.add_rows_to_collection(self.perf_lm, "LM", frequency, data_model)
        return self.perf_lstm, self.perf_cnn, self.perf_lm

#RUN---------------------------------------------------------------------------

if __name__ == "__main__":
    limit_epochs = False
    data = Data()
    data_model = DataModel(data)
    bm = Benchmarking(data = data)
    procs = [] #Dirty but quick
    for dmodel in data_model.available_models:
        for freq in data_model.available_frequencies:
            if limit_epochs: 
                p = mp.Process(target = bm.run, args = (freq, dmodel, {"lstm": {"epochs": limit_epochs}, "cnn" : {"epochs": limit_epochs}, "lm": {}}))
            else:
                p = mp.Process(target = bm.run, args = (freq, dmodel))
            mp.freeze_support() #WINDOWS
            p.start()
            procs.append(p)
        for p in procs:
            p.join()
        for p in procs: #Shared Memory Fix
            try: p.close()
            except: pass
            procs.remove(p)
    bm.export_results("benchmarking_results")