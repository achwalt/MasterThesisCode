#Author------------------------------------------------------------------------

#Benjamin Albrechts
#Helpers to import data consistently across all further analysis

#Import Section----------------------------------------------------------------

import warnings
warnings.simplefilter(action='ignore')

import pandas as pd
import numpy as np
#from types import SimpleNamespace
from datetime import datetime
import multiprocessing as mp

from helpers import load_config, missing_items, pairwise_equal, cols_contain, log_returns
from data_loaders import import_data


#Data Supplier-----------------------------------------------------------------

class Data:
    
    def __init__(self, config = None):
        self.config = load_config(config)
        self.data = self.import_data()
        self.keys = self.data.keys()
        #random.seed = 123
        #self.train_test_indices()
        self.train_test_limits()
        print(">>> Data import complete")
        
    def get_logrets(self):
        logrets = {}
        for key in self.data.keys():
            logrets[key] = np.log(self.data[key]/self.data[key].shift())
        return logrets

    def filter_timestamps(self, data):
        from_date = self.config["data"]["date_range"]["pre_financialization"]["start"]
        try: from_date = datetime.strptime(from_date, "%Y-%m-%d")
        except: pass
        
        to_date = self.config["data"]["date_range"]["post_financialization"]["stop"]
        try: to_date = datetime.strptime(to_date, "%Y-%m-%d")
        except: pass
    
        for key in data.keys():
            if from_date:
                data[key] = data[key].loc(0)[data[key].index >= from_date]
            if to_date:
                data[key] = data[key].loc(0)[data[key].index <= to_date]
        return data

    def drop_unwanted_columns(self, data):
        for item in self.config["data"]["omit_columns"]:
            data_subset = list(item.keys())[0].lower()
            drop_column = item[data_subset].lower()
            for col_name in data[data_subset]:
                if drop_column in col_name.lower():
                    print(f">>> Omitting '{col_name}' as per config")
                    data[data_subset] = data[data_subset].drop(col_name, axis = 1, inplace = False)
        return data

    def __mk_consistent_time__(self, data):
        print("__mk_consistent_time__ is deprecated")
        baseline = data["commodity"].index.to_numpy(dtype = np.int64)
        for key in data.keys():
            print(f">>> Verifying data for consistency: '{key}'")
            comparison = data[key].index.to_numpy(dtype = np.int64) 
            if not pairwise_equal(baseline, comparison):
                missing_dates = missing_items(baseline, comparison, np.int64)
                print(f">>> Inconsistency detected for '{key}' - applying fix")
                for ts in missing_dates:
                    data[key].append(pd.Series(name = datetime.fromtimestamp(ts//10**9).replace(hour = 0, minute = 0, second = 0, microsecond = 0), dtype = np.float64))
            data[key] = data[key].sort_index(axis = 0, ascending = True)
            data[key].ffill(inplace = True)
        
        for key in data.keys():
            try: data_cp = data_cp.join(data[key])
            except: data_cp = data[key]
        
        for key in data.keys():
            data[key] = data_cp.loc(1)[data[key].columns].ffill()
        
        return data
    
    def mk_consistent_time(self, data):
        data_clone = data["commodity"]
        for key in data.keys():
            if key != "commodity":
                data_clone = data_clone.join(data[key])
        for key in data.keys():
            data[key] = data_clone.loc(1)[data[key].columns]
            data[key] = data[key].sort_index(axis = 0, ascending = True).ffill()
        return data

    def import_data(self):
        data = import_data()
        data = self.drop_unwanted_columns(data)
        data = self.filter_timestamps(data)
        data = self.mk_consistent_time(data)
        return data
    
    def train_test_limits(self):
        train = self.config["data"]["train_test_split"]["train"]
        dev = self.config["data"]["train_test_split"]["dev"]
        assert self.config["data"]["train_test_split"]["test"] == round(1 - dev - train, 2), "Invalid config for train/dev/test-split"
        
        index = list(self.data["commodity"].index)
        len_index = len(index)
        self.limit_train = (index[0], index[int(train * len_index)])
        self.limit_dev = (index[int(train * len_index)+1], index[int(train * len_index) + int(dev * len_index)])
        self.limit_test = (index[int(train * len_index) + int(dev * len_index) + 1], index[len_index-1])
    
    
    def consistent_indices(self, x, y):
        x = x.replace(0, np.nan)
        y = y.dropna(how = "all").ffill().dropna(how = "any")
        try: y = y.loc(0)[x.index]
        except: pass
        x = x.loc(0)[y.index].ffill().dropna(how = "any")
        y = y.loc(0)[x.index]
        return x, y
    
    def split_dataframe(self, dataframe, from_date, to_date):
        splitted_data = dataframe.iloc(0)[(dataframe.index >= from_date) & (dataframe.index <= to_date)]
        return splitted_data
    
    def train_test_split(self, dataframe, x_or_y, queue):
        train = self.split_dataframe(dataframe, self.limit_train[0], self.limit_train[1])
        dev = self.split_dataframe(dataframe, self.limit_dev[0], self.limit_dev[1])
        test = self.split_dataframe(dataframe, self.limit_test[0], self.limit_test[1])
        queue[x_or_y] = (train, dev, test)
    
    def train_test_split_xy(self, dict_x_y, randomize = False):
        queue = mp.Manager().dict()
        procs = []
        x, y = self.consistent_indices(dict_x_y["x"], dict_x_y["y"])
        procs.append(mp.Process(target = self.train_test_split, args = (x, "x", queue)))
        procs.append(mp.Process(target = self.train_test_split, args = (y, "y", queue)))
        for p in procs:
            p.daemon = True
            p.start()
        for p in procs:
            p.join()
            p.close()
        return {"x": queue["x"], "y": queue["y"]}

#Data Model--------------------------------------------------------------------
        
class DataModel:
    
    def __init__(self, data_class = None):
        if not data_class: self.data = Data()
        else: self.data = data_class
        self.config = self.data.config
        self.x = None
        self.y = None
        self.current_model = None
        self.current_frequency = None
        self.available_frequencies = ("D", "7D", "M", "Q")
        self.available_models = ("core_model", "extended_model", "large_model")
        self.map = {"core_model": self.core_model, "extended_model": self.extended_model, "large_model": self.large_model}
    
    def resample(self, frequency):
        dataset = self.data.data.copy()
        for key in dataset.keys():
            dataset[key] = dataset[key].resample(frequency).last()
        return dataset
    
    def commodity_log_returns(self, dataframe, frequency):
        if self.config["models"]["returns"]["trading_delay"]:
            over = cols_contain(dataframe, self.config["models"]["returns"]["market_closing_identifier"])
            if frequency == "D":
                under = cols_contain(dataframe, self.config["models"]["returns"]["market_opening_identifier"])
            else:
                under = over
            return np.log(over/under.shift().values).dropna(how = "all")
    
    def log_returns(self, dataframe):
        return np.log(dataframe/dataframe.shift().values)
    
    def consistent_indices(self, x, y):
        x = x.replace(0, np.nan)
        y = y.dropna(how = "all").ffill()
        x = x.loc(0)[y.index].ffill().dropna(how = "any")
        y = y.loc(0)[x.index]
        return x, y
    
    def merge_x(self, temp_data, shift = False):
        for key in temp_data.keys():
            temp_data[key].name = key
            try:
                self.x = self.x.join(temp_data[key])
            except AttributeError:
                self.x  = pd.DataFrame(temp_data[key], index = temp_data[key].index)
        if shift:
            self.x = self.x.shift(shift)
        self.x, self.y = self.consistent_indices(self.x, self.y)
    
    def core_model(self, frequency = None, shift = True):
        if frequency: dataset = self.resample(frequency)
        else:
            dataset = self.data.data.copy()
            frequency = "D"
        
        #Y
        self.y = self.commodity_log_returns(dataset["commodity"], frequency)
        
        #X
        temp_data = {}
        temp_data["dividend_payout_ratio"] = dataset["equity"]["DVD_PAYOUT_RATIO"]/100
        temp_data["default_return_spread"] = log_returns(dataset["corpbond"]["SP5IGBIT_PX_LAST"]) - dataset["govbond"]["USGG30YR_PX_LAST"]/100
        temp_data["default_yield_spread"] = np.log(dataset["corpbond"]["SP5IGBIT_PX_LAST"] / dataset["corpbond"]["SP5HYBIT_PX_LAST"])
        temp_data["dividend_yield"] = dataset["equity"]["GROSS_AGGTE_DVD_YLD"] / 100
        temp_data["inflation"] = dataset["macro"]["CPIYOY_PX_LAST"] / 100
        temp_data["lt_us_govbond_rates"] = dataset["govbond"]["USGG30YR_PX_LAST"] / 100
        temp_data["stock_var"] = log_returns(dataset["equity"]["PX_LAST"]) ** 2
        temp_data["term_spread"] = temp_data["lt_us_govbond_rates"]/100 - dataset["govbond"]["USGG3M_PX_LAST"]/100
        if frequency in ("M", "Q", "Y"):
            temp_data["industry_output_gap"] = log_returns(dataset["macro"]["IPYOY_PX_LAST"])/100
            temp_data["unemployment"] = dataset["unemployment"]["Value"]/100
            temp_data["ads_index"] = dataset["adsindex"]["ADS_Index_050721"]/100
        
        self.x = None
        self.merge_x(temp_data, shift)
        self.current_model = "core_model"
        
        return {"y": self.y, "x": self.x}
    
    def extended_model(self, frequency = None, shift = True):
        self.core_model(frequency, shift = shift)
        if frequency: dataset = self.resample(frequency)
        else:
            dataset = self.data.data.copy()
            frequency = "D"
        temp_data = {}
        temp_data["market_ebitda"] = dataset["equity"]["PX_TO_EBITDA"]/100
        temp_data["return_on_assets"] = dataset["equity"]["RETURN_ON_ASSET"]/100
        temp_data["euro_dollar_fx"] = dataset["macro"]["EURCurncy_PX_LAST"]/100
        #temp_data["momentum"] = self.y.shift()
        
        self.merge_x(temp_data, shift)
        
        self.current_model = "extended_model"
            
        return {"y": self.y, "x": self.x}
    
    def outperformance(self, asset, benchmark):
        return np.log(asset/asset.shift().values) - np.log(benchmark/benchmark.shift().values)
    
    def large_model(self, frequency = None, shift = True):
        self.extended_model(frequency, shift = shift)
        if frequency: dataset = self.resample(frequency)
        else:
            dataset = self.data.data.copy()
            frequency = "D"
        dataset = dataset["mscifac"]
        benchmark = dataset["MXWO_PX_LAST"]
        temp_data = {}
        temp_data["msci_value"] = self.outperformance(dataset["MXWO000V_PX_LAST"], benchmark)
        #temp_data["msci_quality"] = self.outperformance(dataset["MXWOQU_PX_LAST"], benchmark) #Bad data quality
        temp_data["msci_momentum"] = self.outperformance(dataset["MXWOMOM_PX_LAST"], benchmark)
        #temp_data["msci_size"] = self.outperformance(dataset["MXWOSZT_PX_LAST"], benchmark) #Bad data quality
        temp_data["msci_high_div"] = self.outperformance(dataset["MXWDHDVD_PX_LAST"], benchmark)
        temp_data["msci_min_vola"] = self.outperformance(dataset["M1WOMVOL_PX_LAST"], benchmark)
        
        self.merge_x(temp_data, shift)
        
        self.current_model = "large_model"
        
        return {"y": self.y, "x": self.x}


class ContainerPredictedObserved:
    
    def __init__(self):
        self.data = {}
        self.last_item = None
    
    def add_item(self, item):
        self.data[item] = {"predicted": None, "observed": None}
        self.last_item = item
    
    def add_observed(self, observed, item = None):
        if not item: item = self.last_item
        if type(self.data[item]["predicted"]) != type(None):
            assert len(self.data[item]["predicted"]) == len(observed), "Observed and predicted don't have equal length!"
        self.data[item]["observed"] = observed
    
    def add_predicted(self, predicted, item = None):
        if not item: item = self.last_item
        if type(self.data[item]["observed"]) != type(None):
            assert len(self.data[item]["observed"]) == len(predicted), "Observed and predicted don't have equal length!"
        self.data[item]["predicted"] = predicted
    
    def add_index(self, index, item = None, cut_at = "start"):
        if not item: item = self.last_item
        len_diff = len(index) - len(self.data[item]["observed"])
        if len_diff > 0:
            if cut_at == "start":
                index = index[len_diff:]
            elif cut_at == "end":
                index = index[:-len_diff]
        self.data[item] = pd.DataFrame({"observed": self.data[item]["observed"], "predicted": self.data[item]["predicted"]}, index = index)
        
        
#DEBUG-------------------------------------------------------------------------

if __name__ == "__main__":
    data = Data()

if __name__ == "__main__":
    data_model = DataModel(data)
    core_model = data_model.core_model()
    extended_model = data_model.extended_model()
    large_model = data_model.large_model()
    large_model_splitted = data.train_test_split_xy(large_model)
