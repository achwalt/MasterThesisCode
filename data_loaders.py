#Author------------------------------------------------------------------------

#Benjamin Albrechts
#Purpose: Temporary functions to import dataframes

#Import Section----------------------------------------------------------------

import pandas as pd
import multiprocessing as mp

#Functions---------------------------------------------------------------------
def rem_ve(String, listofstrings, filler = ""):
    listofstr = listofstrings
    for i in range(len(listofstr)):
        listofstr[i] = str(listofstr[i]).replace(String, filler)
    return listofstr

#Read data---------------------------------------------------------------------

#Function needs to be exchangeable if data provider changes
#In an ideal 'production' environment, these would need to be exchanged for API-calls on live data

def commodity(queue):
    #Commodities
    metals = pd.read_csv("Data/Metals.csv", sep = ";", encoding = "utf-8-sig")
    metals.iloc[1] = metals.iloc[1].ffill()
    metals.iloc[1] = rem_ve(" ", metals.iloc[1], "_")
    metals.columns = rem_ve("nan_", metals.iloc[1]+"_"+metals.iloc[4])
    metals.drop(list(range(5)), inplace = True)
    metals.index = metals[metals.columns[0]]
    metals.drop("Dates", axis = 1, inplace = True)
    metals.index = pd.to_datetime(metals.index, format = "%d.%m.%Y")
    metals = metals.astype(float)
    
    queue["commodity"] = metals.ffill()

def equity(queue):
    #Equity
    equity_ = pd.read_csv("Data/Equity.csv", sep = ";", encoding = "utf-8-sig")
    equity_.columns = list(range(len(equity_.columns)))
    
    equity_I = equity_[list(range(9))]
    equity_II = equity_[list(range(9,11))]
    equity_III = equity_[list(range(11,len(equity_.columns)))]
    
    del equity_
    
    equity_I.columns = equity_I.iloc[4]
    equity_II.columns = equity_II.iloc[4]
    equity_III.columns = equity_III.iloc[4]
    
    equity_I.drop(list(range(6)), inplace = True)
    equity_II.drop(list(range(6)), inplace = True)
    equity_III.drop(list(range(6)), inplace = True)
    
    equity_I.index = pd.to_datetime(equity_I["Dates"], format = "%d.%m.%Y")
    equity_I.drop("Dates", axis = 1, inplace = True)
    equity_II.index = pd.to_datetime(equity_II["Dates"], format = "%d.%m.%Y")
    equity_II.drop("Dates", axis = 1, inplace = True)
    equity_III.index = pd.to_datetime(equity_III["Dates"], format = "%d.%m.%Y")
    equity_III.drop("Dates", axis = 1, inplace = True)
    
    equity_ = equity_I.join(equity_II).join(equity_III)
    equity_.dropna(how = "all",inplace = True)
    equity_ = equity_.astype(float)
    
    queue["equity"] = equity_.ffill()

def govbond(queue):
    #Government Bonds
    govbond_ = pd.read_csv("Data/GovBonds.csv", sep = ";", encoding = "utf-8-sig")
    govbond_.iloc[2] = govbond_.iloc[2].ffill()
    govbond_.iloc[2] = rem_ve(" ", govbond_.iloc[2])
    govbond_.iloc[2] = rem_ve("Index", govbond_.iloc[2], "")
    govbond_.columns = rem_ve("nan_", govbond_.iloc[2]+"_"+govbond_.iloc[4])
    govbond_.drop(list(range(5)), inplace = True)
    govbond_.index = govbond_[govbond_.columns[0]]
    govbond_.drop("Dates", axis = 1, inplace = True)
    govbond_.index = pd.to_datetime(govbond_.index, format = "%d.%m.%Y")
    
    govbond_ = pd.DataFrame(govbond_)

    govbond_.dropna(how = "all", inplace = True)
    govbond_ = govbond_.astype(float)
    
    queue["govbond"] = govbond_.ffill()

def corpobond(queue):
    #Corporate Bonds
    corpbond_ = pd.read_csv("Data/CorpBonds.csv", sep = ";", encoding = "utf-8-sig")
    corpbond_.iloc[2] = corpbond_.iloc[2].ffill()
    corpbond_.iloc[2] = rem_ve(" ", corpbond_.iloc[2])
    corpbond_.iloc[2] = rem_ve("Index", corpbond_.iloc[2], "")
    corpbond_.columns = rem_ve("nan_", corpbond_.iloc[2]+"_"+corpbond_.iloc[4])
    corpbond_.drop(list(range(5)), inplace = True)
    corpbond_.index = corpbond_[corpbond_.columns[0]]
    corpbond_.drop("Dates", axis = 1, inplace = True)
    corpbond_.index = pd.to_datetime(corpbond_.index, format = "%d.%m.%Y")
    
    corpbond_ = pd.DataFrame(corpbond_)
        
    corpbond_.dropna(how = "all", inplace = True)
    
    corpbond_ = corpbond_.astype(float)
    
    queue["corpbond"] = corpbond_.ffill()

def macro(queue):
    #Macro
    macro_ = pd.read_csv("Data/Macro.csv", sep = ";", encoding = "utf-8-sig")
    macro_.drop(5, inplace = True)
    macro_.iloc[2] = macro_.iloc[2].ffill()
    macro_.iloc[2] = rem_ve(" ", macro_.iloc[2])
    macro_.iloc[2] = rem_ve("Index", macro_.iloc[2], "")
    macro_.columns = rem_ve("nan_", macro_.iloc[2]+"_"+macro_.iloc[4])
    macro_.drop(list(range(5)), inplace = True)
    macro_.index = macro_[macro_.columns[0]]
    macro_.drop("Dates", axis = 1, inplace = True)
    macro_.index = pd.to_datetime(macro_.index, format = "%d.%m.%Y")
    
    macro_ = pd.DataFrame(macro_)
    macro_.dropna(how = "all", inplace = True)
    macro_ = macro_.astype(float)
    
    queue["macro"] = macro_.ffill()

def mscifac(queue):
    #MSCI Factors
    mscifac_ = pd.read_csv("Data/MSCI Factors.csv", sep = ";", encoding = "utf-8-sig")
    mscifac_.drop(5, inplace = True)
    mscifac_.iloc[2] = mscifac_.iloc[2].ffill()
    mscifac_.iloc[2] = rem_ve(" ", mscifac_.iloc[2])
    mscifac_.iloc[2] = rem_ve("Index", mscifac_.iloc[2], "")
    mscifac_.columns = rem_ve("nan_", mscifac_.iloc[2]+"_"+mscifac_.iloc[4])
    mscifac_.drop(list(range(5)), inplace = True)
    mscifac_.index = mscifac_[mscifac_.columns[0]]
    mscifac_.drop("Dates", axis = 1, inplace = True)
    mscifac_.index = pd.to_datetime(mscifac_.index, format = "%d.%m.%Y")
    
    mscifac_ = pd.DataFrame(mscifac_)
    
    mscifac_.dropna(how = "all", inplace = True)
    
    mscifac_ = mscifac_.astype(float)
    
    queue["mscifac"] = mscifac_.ffill()

def adsindex(queue):
    adsindex_ = pd.read_csv("Data/ADS_Index.csv", sep = ";", encoding = "utf-8-sig")
    colnames = list(adsindex_.columns)
    colnames[0] = "Dates"
    adsindex_.columns = colnames
    adsindex_.index = pd.to_datetime(adsindex_.loc(1)["Dates"], format = "%d.%m.%Y")
    adsindex_.dropna(how = "all", inplace = True)
    adsindex_ = pd.DataFrame(adsindex_.loc(1)[adsindex_.columns[-1]], index = adsindex_.index)
    
    queue["adsindex"] = adsindex_.ffill()

def unemployment(queue):
    unemployment_ = pd.read_csv("Data/Unemployment.csv", sep = ",", encoding = "utf-8-sig")
    unemployment_ = unemployment_.loc(0)[unemployment_["LOCATION"] == "USA"]
    unemployment_ = unemployment_.loc(0)[unemployment_["SUBJECT"] == "TOT"]
    unemployment_ = unemployment_.loc(0)[["Q" not in i for i in unemployment_.loc(1)["TIME"]]]
    unemployment_ = unemployment_.loc(0)[["-" in i for i in unemployment_.loc(1)["TIME"]]]
    unemployment_.index = pd.to_datetime(unemployment_.loc(1)["TIME"], format = "%Y-%m")
    queue["unemployment"] = pd.DataFrame(unemployment_.loc(1)["Value"]/100)
    

def import_data():
    """Must be launched in working directory.
    Returns: Metals, Equity, Govbond, Corpbond, Macro, MSCI factors"""
    print(">>> Importing data...")
    procs = []
    queue = mp.Manager().dict()
    
    procs.append(mp.Process(target = commodity, args = [queue]))
    procs.append(mp.Process(target = equity, args = [queue]))
    procs.append(mp.Process(target = govbond, args =[queue]))
    procs.append(mp.Process(target = corpobond, args = [queue]))
    procs.append(mp.Process(target = macro, args = [queue]))
    procs.append(mp.Process(target = mscifac, args = [queue]))
    procs.append(mp.Process(target = adsindex, args = [queue]))
    procs.append(mp.Process(target = unemployment, args = [queue]))
    
    for p in procs:
        p.daemon = True
        p.start()
    
    for p in procs:
        p.join()
        p.close()

    ret = {}
    for key in queue.keys():
        ret[key] = queue[key]

    return ret