#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

from stargazer.stargazer import Stargazer
from datetime import datetime
import os
from werkzeug.utils import secure_filename

from data import Data, DataModel
from helpers import latex_table
from linear_models import LinearModels

#Main--------------------------------------------------------------------------

class EquityIntegration:
    
    def __init__(self, data = None):
        if not data: self.data = Data()
        else: self.data = data
        self.config = self.data.config
        self.data_model = DataModel(self.data)
        self.linear_models = LinearModels(self.config)
        #For lazy programmers
        self.pre_fin = self.pre_financialization
        self.post_fin = self.post_financialization

    def pre_financialization(self):
        data = self.data_model.core_model("M", True)
        x = data["x"].loc(0)[data["x"].index <= datetime.strptime(self.config["data"]["date_range"]["pre_financialization"]["stop"], "%Y-%m-%d")]
        y = data["y"].loc(0)[data["y"].index <= datetime.strptime(self.config["data"]["date_range"]["pre_financialization"]["stop"], "%Y-%m-%d")]
        models = self.linear_models.ols(x, y)
        return {"x": x, "y": y, "models": models}
    
    def post_financialization(self):
        data = self.data_model.core_model("M", True)
        x = data["x"].loc(0)[data["x"].index >= datetime.strptime(self.config["data"]["date_range"]["post_financialization"]["start"], "%Y-%m-%d")]
        y = data["y"].loc(0)[data["y"].index >= datetime.strptime(self.config["data"]["date_range"]["post_financialization"]["start"], "%Y-%m-%d")]
        models = self.linear_models.ols(x, y)
        return {"x": x, "y": y, "models": models}
    
    def export_regressions(self, models, fname, caption = None):
        export = Stargazer([models[key] for key in models.keys()])
        export.dep_var_name = f"{fname}: "
        export.custom_columns(list(models.keys()), [1 for i in range(len(models.keys()))])
        export.significant_digits(4)
        latex = os.path.join(self.config["export_folder"]["main"], self.config["export_folder"]["tables"]["main"], self.config["export_folder"]["tables"]["latex"])
        html = os.path.join(self.config["export_folder"]["main"], self.config["export_folder"]["tables"]["main"], self.config["export_folder"]["tables"]["html"])
        try: os.makedirs(latex)
        except: pass
        try: os.makedirs(html)
        except: pass
        with open(os.path.join(latex, fname + ".tex"), "w") as f:
            f.write(latex_table(export.render_latex(), caption))
        with open(os.path.join(html, fname + ".html"), "w") as f:
            f.write(export.render_html())
    
    def run_regressions(self, export = True, show = False):
        pre_fin = self.pre_fin()["models"]
        post_fin = self.post_fin()["models"]
        
        if show:
            for key in pre_fin.keys():
                print(f"{key}: Pre Financialization")
                print(pre_fin[key].summary())   
            for key in post_fin.keys():
                print(f"{key}: Post Financialization")
                print(post_fin[key].summary())
                
        if export:
            self.linear_models.export_regressions(pre_fin, fname = "pre_financialization_regression_output", caption = "Core\\_Model Pre-Financialization")
            self.linear_models.export_regressions(post_fin, fname = "post_financialization_regression_output", caption = "Core\\_Model Post-Financialization")
        
#DEBUG-------------------------------------------------------------------------

if __name__ == "__main__":
    data = Data()
    equ_int = EquityIntegration(data)
    equ_int.run_regressions(export = True, show = True)
