#Author------------------------------------------------------------------------

#Benjamin Albrechts

#This is beautified prototyping only

#Import Section----------------------------------------------------------------

from data import Data, DataModel
from plots import Plots
from helpers import cols_contain, cols_not_contain, capitalize, StandardScaler, MaxScaler

class FeatureEngineering:
    
    def __init__(self, data = None, data_model = None):
        self.data = Data() if not data else data
        self.config = self.data.config
        self.data_model = DataModel(self.data) if not data_model else data_model
        self.plots = Plots(config = self.config)
        self.topfolder = "FeatureEngineering"
        
    def plot_features(self, resample = "M"):
        for model in self.data_model.available_models:
            x = StandardScaler(self.data_model.map[model](resample)["x"]).transform()
            self.plots.data_frame(dataframe = x,
                                  title = f"Standardized features for {model}",
                                  x_label = "", y_label = "",
                                  #legend = list(x.columns), legend_loc = 0,
                                  subtext = (0.20, 0.83, "Stationary features transformed by StandardScaler"),
                                  fname = f"{model}_{resample}_feature_timeseries", topfolder = self.topfolder)
    
    def corr_features(self, resample = "M"):
        for model in self.data_model.available_models:
            x = StandardScaler(self.data_model.map[model](resample)["x"]).transform()
            self.plots.correlation_matrix(dataframe = x,
                               title = f"Correlation heatmap for '{model}'",
                               fname = f"{model}_{resample}_feature_corr_heatmap",
                               topfolder = self.topfolder)
    
    def corr_assets(self, resample = "D"):
        x = StandardScaler(self.data_model.map["core_model"](resample)["y"]).transform()
        self.plots.correlation_matrix(dataframe = x,
                               title = "Correlation heatmap for included assets",
                               fname = f"{resample}_asset_corr_heatmap",
                               topfolder = self.topfolder)
    
    def price_vol(self, resample = "D"):
        dataset = self.data.data["commodity"].ffill().dropna()
        for comdty in self.data_model.core_model()["y"].columns:
            comdty = comdty.split("_")[0].lower()
            self.plots.px_vol_plot(cols_contain(dataset, comdty),
                                   price_identifier = ["px_last"],
                                   volume_identifier = ["px_volume"],
                                   title = f"{capitalize(comdty)}",
                                   legend_loc = 0,
                                   fname = f"{comdty}_prices_volumes",
                                   topfolder = self.topfolder)
            
    def price_vol_reg(self, resample = "M"):
        for comdty in self.data_model.core_model()["y"].columns:
            comdty = comdty.split("_")[0]
            self.plots.regression_plot(self.data.data["commodity"],
                                       f"{comdty}_PX_VOLUME",
                                       f"{comdty}_PX_LAST",
                                       title = f"{comdty} | Volume vs. Price",
                                       shift_x = 0,
                                       make_stationary = False,
                                       fname = f"{comdty}_price_vol_reg",
                                       topfolder = self.topfolder)
            
    def logret_density(self, resample = "D"):
        dataset = self.data_model.core_model(resample)["y"]
        for comdty in dataset.columns:
            self.plots.density_plot(dataframe = dataset.loc(1)[comdty],
                                   title = f"{capitalize(comdty)} | Density | Log returns",
                                   add_random_normal = True,
                                   fname = f"{comdty}_logret_density",
                                   topfolder = self.topfolder)
        
    def run(self):
        self.plot_features()
        self.corr_features()
        self.corr_assets()
        self.price_vol()
        self.price_vol_reg()
        self.logret_density()

#Run it------------------------------------------------------------------------

if __name__ == "__main__":
    data = Data()
    data_model = DataModel(data)
    fe = FeatureEngineering(data, data_model)
    fe.run()
