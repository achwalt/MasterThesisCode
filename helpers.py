#Meta--------------------------------------------------------------------------

#Benjamin Albrechts
#Purpose: Various helpers to be imported

#Import------------------------------------------------------------------------

import os
from numba import jit
import numpy as np
import pandas as pd
import yaml
import multiprocessing as mp
import json

#Main--------------------------------------------------------------------------

def load_config(config = None):
    if not config:
        with open("config.yml", "r") as f:
            config = yaml.safe_load(f)
    return config


@jit(nopython = True, nogil = True)
def missing_items(array_look_for, array, dtype):
    missing = np.empty(0, dtype = dtype)
    for item in array_look_for:
        for item_ in array:
            if item == item_:
                break
            elif item_ == array[-1]:
                missing_copy = np.empty(len(missing) + 1, dtype = dtype)
                for i in range(len(missing)):
                    missing_copy[i] = missing[i]
                missing_copy[len(missing)] = item
                missing = missing_copy
    return missing


@jit(nopython = True, nogil = True)
def and_(array):
    for item in array:
        if item != True:
            return False
    return True


@jit(nopython = True, nogil = True)
def pairwise_equal(array1, array2):
    if len(array1) != len(array2):
        return False
    else:
        for i in range(len(array1)):
            if array1[i] != array2[i]:
                return False
    return True


@jit(nopython = True, nogil = True)
def array1_in_array2(array1, array2):
    results = np.empty(array1.shape, dtype = np.int8)
    for i in range(len(array1)):
        if array1[i] in array2:
            results[i] = 1
        else:
            results[i] = 0
    return results


def cols_contain(dataframe, list_of_strings):
    colnames = []
    if type(list_of_strings) == str:
        list_of_strings = [list_of_strings]
    for string in list_of_strings:
        for col in dataframe.columns:
            if string.lower() in col.lower() and col not in colnames:
                colnames.append(col)
    return dataframe.loc(1)[colnames]


def cols_not_contain(dataframe, list_of_strings):
    colnames = []
    if type(list_of_strings) == str:
        list_of_strings = [list_of_strings]
    for string in list_of_strings:
        for col in dataframe.columns:
            if string.lower() not in col.lower() and col not in colnames:
                colnames.append(col)
    return dataframe.loc(1)[colnames]


def log_returns(dataframe):
    return np.log(dataframe/dataframe.shift().values)


def pb(current, total, desc = ""):
    print("", end = "\r")
    multiply = int(round(current/total*100, 0))
    done = "|" * multiply
    remaining = "-" * (100 - multiply)
    print(f"[{multiply}%] " + desc + ": [" + done + remaining + "]", end = "\r")
    print("", end = "\r")


def latex_table(latex_string, caption = None):
    latex_split = latex_string.replace("_", "\\_").split("\n")
    latex_string = ""
    for line in latex_split:
        if line.startswith("\\begin{tabular}"):
            latex_string += "\\resizebox{\\textwidth}{!}{%\n" + line
        elif line.startswith("\end{tabular}"):
            if not caption:
                latex_string += line + "\n}"
            else:
                latex_string += line + "\n}\n\\caption{" + caption + "}"
        else:
            latex_string += line
        latex_string += "\n"
    return latex_string.strip()


class StandardScaler:
    
    def __init__(self, dataframe):
        self.data = dataframe
        self.mean = self.data.mean()
        self.std = self.data.std()
        
    def transform(self, new_data = None):
        if type(new_data) == pd.core.frame.DataFrame:
            return (new_data - self.mean)/self.std
        else:
            return (self.data - self.mean)/self.std
    
    def reverse(self, new_data = None):
        if type(new_data) == pd.core.frame.DataFrame:
            return new_data * self.std + self.mean
        else:
            return self.data
 
        
class MaxScaler:
    
    def __init__(self, dataframe):
        self.data = dataframe
        self.max = self.data.max()
        
    def transform(self, new_data = None):
        if type(new_data) == pd.core.frame.DataFrame:
            return new_data / self.max
        else:
            return self.data / self.max
    
    def reverse(self, new_data = None):
        if type(new_data) == pd.core.frame.DataFrame:
            return new_data * self.max
        else:
            return self.data
  
        
def translate_resampling(string):
    if string == None or string == "D":
        return "Daily"
    elif string == "7D" or string == "W":
        return "Weekly"
    elif string == "M":
        return "Monthly"
    elif string == "Q":
        return "Quarterly"
    elif string == "Y":
        return "Yearly"
    else:
        raise KeyError(f"Frequency '{string}' is unknown")


def translate_commodity(string):
    ci = string.lower()
    if "gold" in ci:
        return "Gold"
    elif "silver" in ci:
        return "Silver"
    elif "copper" in ci:
        return "Copper"
    elif "platin" in ci:
        return "Platin"
    elif "palladium" in ci:
        return "Palladium"
    else:
        raise KeyError(f"Commodity identifier '{string}' unknown")


def capitalize(string, separators = (" ", "-", "_", "/", "\\")):
    cap_str = string
    for sep in separators:
        if sep in cap_str:
            split_str = cap_str.split(sep)
            cap_str = ""
            for s in split_str:
                if s:
                    s = s[0].upper() + s[1:]
                    cap_str += s + sep
            cap_str = cap_str[:-1]
    return cap_str[0].upper() + cap_str[1:].strip()


def mkdir(path):
    folder, file = os.path.split(path)
    if folder:
        try: os.makedirs(folder)
        except: pass
    return path


#@jit(nopython = True, nogil = True) #Function works with jit, but it is incompatible with mp
def log_change(array, ascending = True, incl_na = False):
    result = np.empty(len(array))
    for i in range(len(array)):
        if ascending:
            if i > 0: result[i] = np.log(array[i]/array[i-1])
            elif incl_na: result[i] = np.nan
        else:
            if i < len(array)-1: result[i] = np.log(array[i]/array[i+1])
            elif incl_na: result[i] = np.nan
    if incl_na: return result
    else: return result[1:] if ascending else result[:-1]


class ScaleHyperparameters:
    
    def __init__(self, model_class, data, export_json_fpath = None, export_yaml_fpath = None, data_model = "core_model", frequency = None, scaler = "standard", commodities = []):
        self.data = data
        self.config = self.data.config
        self.data_model = data_model
        self.frequency = frequency
        self.scaler = scaler
        self.commodities = commodities
        self.model_class = model_class
        self.export_json_fpath = export_json_fpath
        if export_json_fpath:
            directory, fname = os.path.split(export_json_fpath)
            try: os.makedirs(directory)
            except: pass
        self.export_yaml_fpath = export_yaml_fpath
        if export_yaml_fpath:
            directory, fname = os.path.split(export_yaml_fpath)
            try: os.makedirs(directory)
            except: pass
    
    def train_process(self, custom_specification, comdty, queue, best_other_params):
        parameter = [custom_specification[k] for k in custom_specification.keys()][0]
        if best_other_params[comdty]:
            for key in best_other_params[comdty].keys():
                custom_specification[key] = best_other_params[comdty][key]
        model = self.model_class(self.data)
        #model.config["models"]["cnn"]["sideload_hyperparams"]["activate"] = False
        model.hyperparams_tuning = True
        container_object = model.run(frequency = self.frequency, data_model = self.data_model, commodities = [comdty], scaler = self.scaler, custom_specification = custom_specification)
        observed, predicted = container_object.data[comdty]["observed"], container_object.data[comdty]["predicted"]
        correl = np.corrcoef(observed, predicted)[0][1]
        history = model.current_history.history
        hist_train = np.array(history["least_squares"])
        hist_dev = np.array(history["val_least_squares"])
        improvements_trainset = np.sum(log_change(hist_train))
        improvements_devset = np.sum(log_change(hist_dev))
        total_epochs = len(history["least_squares"])
        queue.append((parameter, correl, improvements_trainset, improvements_devset, total_epochs))
    
    def extract_queue(self, queue):
        max_ = -2
        hyperparam = None
        for item in queue:
            if item[1] > max_:
                if (self.config["hyperparameter_autotuning"]["require_val_error_improvements"] <= -item[2]) or not self.config["hyperparameter_autotuning"]["require_val_error_improvements"]:
                    if (self.config["hyperparameter_autotuning"]["require_train_error_improvements"] <= -item[3]) or not self.config["hyperparameter_autotuning"]["require_train_error_improvements"]:
                        if (self.config["hyperparameter_autotuning"]["require_training_for_min_epochs"] <= item[4]) or not self.config["hyperparameter_autotuning"]["require_training_for_min_epochs"]:
                            max_ = item[1]
                            hyperparam = item[0]
        return hyperparam
    
    def run(self, custom_hyperparameters):
        limit = min(max(mp.cpu_count()-1, 1), self.config["hyperparameter_autotuning"]["max_parallel_processes"])
        best_parameters = {}
        for comdty in self.commodities:
            best_parameters[comdty] = {}
            for key in custom_hyperparameters:
                procs = []
                queue = mp.Manager().list()
                for entry in custom_hyperparameters[key]:
                    custom_specification = {key: entry}
                    p = mp.Process(target = self.train_process, args = (custom_specification, comdty, queue, best_parameters))
                    procs.append(p)
                    #mp.freeze_support()
                    p.start()
                    while len(procs) > limit:
                        for p in procs:
                            p.join(timeout = 0.001)
                            if not p.is_alive():
                                p.close()
                                procs.remove(p)
                for p in procs:
                    p.join()
                for p in procs: #Shared Memory Fix
                    p.close()
                    procs.remove(p)
                
                hyperparam = self.extract_queue(queue)
                if hyperparam:
                    best_parameters[comdty][key] = hyperparam
                
                if self.export_json_fpath:
                    with open(self.export_json_fpath, "w", encoding = "utf-8-sig") as f:
                        f.write(json.dumps(best_parameters))
                if self.export_yaml_fpath:
                    with open(self.export_yaml_fpath, "w", encoding = "utf-8-sig") as f:
                        yaml.dump(best_parameters, f)
        return best_parameters

    
def change_colnames(df, colnames):
    assert type(colnames) == list or type(colnames) == tuple, "Please supply list or tuple of colnames"
    dataframe = df
    dataframe.columns = colnames
    return dataframe