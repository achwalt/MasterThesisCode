#Meta--------------------------------------------------------------------------

#Benjamin Albrechts
#Purpose: OLS Helper

#Import------------------------------------------------------------------------

import statsmodels.api as sm
from stargazer.stargazer import Stargazer
import os
import numpy as np

from helpers import load_config, StandardScaler, MaxScaler, latex_table
from data import ContainerPredictedObserved, DataModel

#Main--------------------------------------------------------------------------

class LinearModels:
    
    def __init__(self, config = None, data = None, data_model = None):
        self.config = config if not data else data.config
        if not self.config:
            self.config = load_config()
        self.current_models = None
        self.models = self.current_models #TODO: Fix me (compatibility)
        self.current_frequency = None
        self.current_datamodel = None
        self.model_collection = {}
        self.x_scaler = None
        self.y_scaler = None
        self.data = data
        self.data_model = data_model if data_model else DataModel(data_class = self.data) if self.data else None
        self.scaler_type = None

    def ols(self, x, y, intercept = True, cov_type = "hc0", scaler = None):
        
        if scaler == "standard":
            self.x_scaler = StandardScaler(x)
            self.y_scaler = StandardScaler(y)
        elif scaler == "max":
            self.x_scaler = MaxScaler(x)
            self.y_scaler = MaxScaler(y)
        if scaler:
            x = self.x_scaler.transform()
            y = self.y_scaler.transform()
        
        self.current_models = {}
        for y_ in y.columns:
            if intercept:
                x = sm.add_constant(x)        
            model = sm.OLS(y.loc(1)[y_].ravel(), x)
            fitted = model.fit(cov_type = cov_type)
            self.current_models[y_] = fitted
        self.models = self.current_models #TODO: Fix me (compatibility)
        return self.current_models

    def export_regressions(self, models = None, fname = None, custom_header = None, caption = None):
        assert fname, "You must specify a filename"
        if not models:
            models = self.current_models
        export = Stargazer([models[key] for key in models.keys()])
        if custom_header:
            export.dep_var_name = f"{custom_header}: "
        else:
            export.dep_var_name = None
        export.custom_columns(list(models.keys()), [1 for i in range(len(models.keys()))])
        export.significant_digits(4)
        export.confidence_intervals = True
        latex = os.path.join(self.config["export_folder"]["main"], self.config["export_folder"]["tables"]["main"], self.config["export_folder"]["tables"]["latex"])
        html = os.path.join(self.config["export_folder"]["main"], self.config["export_folder"]["tables"]["main"], self.config["export_folder"]["tables"]["html"])
        try: os.makedirs(latex)
        except: pass
        try: os.makedirs(html)
        except: pass
        latex_fpath = os.path.join(latex, fname + ".tex")
        html_fpath = os.path.join(html, fname + ".html")
        with open(latex_fpath, "w") as f:
            f.write(latex_table(export.render_latex(), caption))
        with open(html_fpath, "w") as f:
            f.write(export.render_html())
        return html_fpath, latex_fpath
    
    def rescale(self, np_array, commodity_identifier):
        if self.scaler_type == "standard":
            return np.add(np.multiply(np_array, self.y_scaler.std[commodity_identifier]), self.y_scaler.mean[commodity_identifier])
        elif self.scaler_type == "max":
            return np.multiply(np_array, self.y_scaler.max[commodity_identifier])
    
    def test(self, x_test, y_test, intercept = True):
        index = y_test.index.to_list()
        x_scaled = self.x_scaler.transform(x_test)
        if intercept:
            x_scaled = sm.add_constant(x_scaled)
        y_scaled = self.y_scaler.transform(y_test)
        container = ContainerPredictedObserved()
        for commodity_identifier in self.current_models.keys():
            prediction_ = self.current_models[commodity_identifier].predict(x_scaled)
            container.add_item(commodity_identifier)
            container.add_observed(self.rescale(y_scaled.loc(1)[commodity_identifier].values, commodity_identifier))
            container.add_predicted(self.rescale(prediction_.values, commodity_identifier))
            container.add_index(index)
        return container
    
    def run(self, frequency = None, data_model = "core_model", commodities = [], scaler = "standard", return_predictions = True, return_models = True, custom_specification = {}, shift = 1):
        #One more reason why Pandas should be avoided entirely
        if frequency == "W": frequency = "7D"
        
        try: data_model_ = self.data_model.map[data_model](frequency = frequency, shift = shift)
        except: raise NameError("LinearModels must be initialized with 'Data' class")
        
        if commodities:
            data_model_["y"] = data_model_["y"].loc(1)[commodities]
        
        self.scaler_type = scaler
        
        self.current_frequency = frequency
        self.current_datamodel = data_model
        self.model_collection[data_model] = {}
        self.model_collection[data_model][frequency] = {}
        
        self.dataset = self.data.train_test_split_xy(data_model_, False)
        x_train, x_dev, x_test = self.dataset["x"]
        y_train, y_dev, y_test = self.dataset["y"]
        
        intercept = custom_specification["intercept"] if custom_specification else True
        cov_type = custom_specification["cov_type"] if custom_specification else "hc0"
        self.model_collection[data_model][frequency] = self.ols(x = x_train, y = y_train, intercept = intercept, cov_type = cov_type, scaler = scaler)
        
        if return_predictions:
            predictions = self.test(x_test, y_test, intercept)
        
        if return_models:
            if return_predictions:
                return self.model_collection, predictions
            else:
                return self.model_collection
        else:
            if return_predictions:
                return predictions