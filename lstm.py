#Meta--------------------------------------------------------------------------

#Benjamin Albrechts
#Purpose: Long Short Term Memory Handler

#Import------------------------------------------------------------------------

#from matplotlib import pyplot
from numba import jit
import numpy as np
import pandas as pd
import multiprocessing as mp
#import ray
#ray.init()
import yaml
import json

import tensorflow as tf
tf.autograph.set_verbosity(3)
tf.get_logger().setLevel('FATAL')
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.FATAL)

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.optimizers.schedules import ExponentialDecay
#from tensorflow.keras.initializers import RandomUniform, RandomNormal
#from tensorflow.keras import backend as kerasBackend
from tensorflow.keras.layers import LSTM

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
 
import random

from data import Data, DataModel, ContainerPredictedObserved
from helpers import StandardScaler, MaxScaler, translate_resampling, translate_commodity, capitalize, cols_contain, ScaleHyperparameters, load_config
from plots import Plots

#Main--------------------------------------------------------------------------

#@ray.remote
class Lstm:
    
    def __init__(self, data = None):
        if not data:
            self.data = Data()
        else:
            self.data = data
        self.data_model = DataModel(self.data)
        self.config = self.data.config
        
        self.x_scaler = None
        self.y_scaler = None
        self.scaler_type = None
        self.model_collection = {}
        self.history = {}
        
        self.current_datamodel = None
        self.current_frequency = None
        self.current_models = None
        self.current_history = None
        
        self.custom_specification = None
        self.hyperparams_tuning = False
        
        with open("lstm_hyperparams.yml", "r", encoding = "utf-8-sig") as f:
            self.hyperparams = yaml.safe_load(f)
    
    def fix_seed(self, seed_value):
        "Fixes a lot of random influences in Keras and its backend Tensorflow"    
        os.environ['PYTHONHASHSEED'] = str(seed_value)
        random.seed = seed_value
        np.random.seed(seed_value)
        tf.random.set_seed(seed_value)
        #(Seed for Keras Backend => TensorFlow Global)
        session_conf = tf.compat.v1.ConfigProto(intra_op_parallelism_threads = 1, inter_op_parallelism_threads = 1)
        sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=session_conf)
        tf.compat.v1.keras.backend.set_session(sess)
        
    def lag_leads(self, data, n_in = 1, n_out = 1, dropnan = True):
        k_vars = 1 if type(data) == list else data.shape[1]
        df = pd.DataFrame(data)
        cols, names = [], []
        for i in range(n_in, 0, -1):
            cols.append(df.shift(i))
            names += [str(data.columns[j]) + "_t-" + str(i) for j in range(k_vars)]
        for i in range(0, n_out):
            cols.append(df.shift(-i))
            if i == 0:
                names += [str(data.columns[j]) for j in range(k_vars)]
            else:
                names += [str(data.columns[j]) + "_t+" + str(i) for j in range(k_vars)]
        everything = pd.concat(cols, axis=1)
        everything.columns = names
        if dropnan:
            everything.dropna(inplace = True)
        return everything
    
    def to_3d(self, dataframe, lags, n_features):
        return dataframe.values.reshape([len(dataframe.index), lags + 1, n_features])
    
    def least_squares(self, true, pred):
        "Euclidian distance"
        return ((true - pred)**2)

    def least_squared_distance(self, true, pred):
        "Mean of Squared Distance and Total deviation"
        return abs((true - pred)/true) * self.least_squares(true, pred)
   
    def model_specification(self, commodity_identifier):
        specification = self.hyperparams[self.current_datamodel][translate_resampling(self.current_frequency).lower()][translate_commodity(commodity_identifier).lower()]
        if self.config["models"]["lstm"]["sideload_hyperparams"]["activate"]:
            fending = self.config["models"]["lstm"]["sideload_hyperparams"]["file_ending"]
            sload_name = self.fname_sideloaded_lstm_autotuning(data_model = self.current_datamodel, frequency = self.current_frequency, config = self.config, fending = fending)
            try:
                with open(sload_name, "r", encoding = "utf-8-sig") as f:
                    if fending.endswith("yml") or fending.endswith("yaml"):
                        sload_hyperparams = yaml.safe_load(f)
                    elif fending.endswith("json"):
                        sload_hyperparams = json.load(f)
                    else: raise NameError(f"Unknown fileending '{fending}' - Use 'json', 'yml' or 'yaml'")
            except FileNotFoundError: sload_hyperparams = None
            if sload_hyperparams:
                for key in sload_hyperparams.keys():
                    if commodity_identifier.lower() in key.lower():
                        sload_hyperparams = sload_hyperparams[key]
                        for key in sload_hyperparams.keys():
                            specification[key] = sload_hyperparams[key]
                        break
        if self.custom_specification:
            for key in self.custom_specification.keys():
                specification[key] = self.custom_specification[key]
        return specification
   
    def prepare_data(self, x_scaled, y_scaled, commodity_identifier, specification):
        n_features = len(x_scaled.columns)
        
        x = self.lag_leads(x_scaled, specification["lags"], 1, True)
        x, y = self.data.consistent_indices(x, y_scaled)
        x = self.to_3d(x, specification["lags"], n_features)
        y = y.loc(1)[commodity_identifier].values
        
        return n_features, x, y
    
    def model(self, x_train_scaled, y_train_scaled, x_dev_scaled, y_dev_scaled, commodity_identifier):
        
        specification = self.model_specification(commodity_identifier)
        
        n_features, x, y = self.prepare_data(x_train_scaled, y_train_scaled, commodity_identifier, specification)
        _, x_dev, y_dev = self.prepare_data(x_dev_scaled, y_dev_scaled, commodity_identifier, specification)
        
        print(f">>> Shapes | X: {x.shape} | Y: {y.shape}")
        #Train the model
        self.fix_seed(specification["seed"])
        epochs = specification["epochs"] if not self.hyperparams_tuning else self.config["hyperparameter_autotuning"]["max_epochs"]
        model = Sequential()
        model.add(LSTM(n_features,
                       input_shape = (x.shape[1], x.shape[2]),
                       activation = specification["activation"],
                       recurrent_activation = specification["recurrent_activation"],
                       use_bias = specification["use_bias"],
                       bias_initializer = specification["bias_initializer"],
                       bias_regularizer = specification["bias_regularizer"],
                       bias_constraint = specification["bias_constraint"],
                       kernel_initializer = specification["kernel_initializer"],
                       kernel_regularizer = specification["kernel_regularizer"],
                       kernel_constraint = specification["kernel_constraint"],
                       recurrent_initializer = specification["recurrent_initializer"],
                       recurrent_regularizer = specification["recurrent_regularizer"],
                       recurrent_constraint = specification["recurrent_constraint"],
                       recurrent_dropout = specification["recurrent_dropout"],
                       activity_regularizer = specification["activity_regularizer"],
                       unit_forget_bias = specification["unit_forget_bias"],
                       dropout = specification["dropout"],
                       return_sequences = specification["return_sequences"],
                       return_state = specification["return_state"],
                       go_backwards = specification["go_backwards"],
                       stateful = specification["stateful"],
                       time_major = specification["time_major"],
                       unroll = specification["unroll"],
                       ))
        model.add(Dense(1, activation = "elu"))
        #model.compile(loss = 'mae', optimizer = 'adam')
        learning_rate = ExponentialDecay(initial_learning_rate = self.config["models"]["lstm"]["initial_learning_rate"], decay_steps = specification["epochs"], decay_rate = self.config["models"]["lstm"]["learning_decay_rate"])
        opt = SGD(learning_rate = learning_rate, momentum = self.config["models"]["lstm"]["learning_momentum"])
        #opt = tf.keras.optimizers.Adam(learning_rate=0.1)
        model.compile(optimizer = opt, loss = self.least_squares, metrics = [self.least_squares])
        early_stop_val = tf.keras.callbacks.EarlyStopping(monitor = 'val_least_squares', patience = self.config["models"]["lstm"]["early_stop_val_patience"])
        early_stop = tf.keras.callbacks.EarlyStopping(monitor = 'least_squares', patience = self.config["models"]["lstm"]["early_stop_train_patience"])
        # fit network
        self.current_history = model.fit(x, y, epochs = epochs, batch_size = self.config["models"]["lstm"]["batch_size"], validation_data = (x_dev, y_dev), verbose = 1, shuffle = self.config["models"]["lstm"]["shuffle"], callbacks = [early_stop, early_stop_val])
        self.history[commodity_identifier] = self.current_history
        return model
    
    def train(self, x_train, y_train, x_dev, y_dev, scaler = "standard"):
        assert type(self.current_datamodel) != type(None), f"Please specify your datamodel first: {self.data_model.available_models}"
        assert type(self.current_frequency) != type(None), f"Please specify your resampling frequency first: {self.data_model.available_frequencies}"
        
        if scaler == "standard":
            self.x_scaler = StandardScaler(x_train)
            self.y_scaler = StandardScaler(y_train)
        elif scaler == "max":
            self.x_scaler = MaxScaler(x_train)
            self.y_scaler = MaxScaler(y_train)
        else:
            assert False, "Scaler only supports 'standard' or 'max'"
        
        self.scaler_type = scaler
        
        x = self.x_scaler.transform()
        y = self.y_scaler.transform()
        
        x_cross_valid = self.x_scaler.transform(x_dev)
        y_cross_valid = self.y_scaler.transform(y_dev)
        
        self.current_models = {}
        for commodity_identifier in y.columns:
            self.current_models[commodity_identifier] = self.model(x, y, x_cross_valid, y_cross_valid, commodity_identifier)
            self.model_collection[self.current_datamodel][self.current_frequency][commodity_identifier] = self.current_models[commodity_identifier]
            
        return self.current_models
    
    def rescale(self, np_array, commodity_identifier):
        if self.scaler_type == "standard":
            return np.add(np.multiply(np_array, self.y_scaler.std[commodity_identifier]), self.y_scaler.mean[commodity_identifier])
        elif self.scaler_type == "max":
            return np.multiply(np_array, self.y_scaler.max[commodity_identifier])
    
    def test(self, x_test, y_test):
        index = y_test.index.to_list()
        x_scaled = self.x_scaler.transform(x_test)
        y_scaled = self.y_scaler.transform(y_test)
        container = ContainerPredictedObserved()
        for commodity_identifier in self.current_models.keys():
            specification = self.model_specification(commodity_identifier)
            n_features, x, y = self.prepare_data(x_scaled, y_scaled, commodity_identifier, specification)
            prediction_ = self.current_models[commodity_identifier].predict(x)
            container.add_item(commodity_identifier)
            container.add_observed(self.rescale(y, commodity_identifier))
            container.add_predicted(self.rescale(prediction_.reshape(y.shape), commodity_identifier))
            container.add_index(index)
        return container
    
    def run(self, frequency = None, data_model = "core_model", commodities = [], scaler = "standard", return_predictions = True, return_models = True, custom_specification = {}, shift = 1):
        #One more reason why Pandas should be avoided entirely
        if frequency == "W": frequency = "7D"
        
        data_model_ = self.data_model.map[data_model](frequency = frequency, shift = shift)
        
        if commodities:
            data_model_["y"] = data_model_["y"].loc(1)[commodities]
        
        self.custom_specification = custom_specification
        
        self.current_frequency = frequency
        self.current_datamodel = data_model
        self.model_collection[data_model] = {}
        self.model_collection[data_model][frequency] = {}
        
        self.dataset = self.data.train_test_split_xy(data_model_, False)
        x_train, x_dev, x_test = self.dataset["x"]
        y_train, y_dev, y_test = self.dataset["y"]
        
        model_collection = self.train(x_train, y_train, x_dev, y_dev, scaler = scaler)
        if self.hyperparams_tuning:
            return self.test(pd.concat([x_train, x_dev]), pd.concat([y_train, y_dev]))
        if return_predictions: predictions = self.test(x_test, y_test)
        if return_models:
            if return_predictions:
                return model_collection, predictions
            else:
                return model_collection
        else:
            if return_predictions:
                return predictions

    @staticmethod
    def fname_sideloaded_lstm_autotuning(data_model, frequency, config = None, fending = "yml"):
        assert fending == "yml" or fending == "yaml" or fending == "json", "Choose 'yml', 'yaml' or 'json' for fending"
        config = load_config() if not config else config
        export_fname = os.path.join(config["export_folder"]["main"], config["export_folder"]["hyperparameters"]["main"], config["export_folder"]["hyperparameters"]["lstm"])
        export_fname = os.path.join(export_fname, f'{config["export_folder"]["hyperparameters"]["lstm"]}_{data_model}_{frequency}.yml')
        return export_fname


#Scalable Hyperparameter Tuning------------------------------------------------

def autotune_lstm(data = None, data_model = None, test_params = None, limit_datamodels = None, limit_frequencies = None):
    if not data: data = Data()
    if not data_model: data_model = DataModel(data)
    
    if not test_params: test_params = {"seed": tuple(range(0, 101, 5)),
                                       "lags": tuple(range(1, 5)),
                                       "recurrent_activation": ["tanh", "elu"],
                                       "use_bias": (True, False),
                                       "unit_forget_bias": (True, False),
                                       #"kernel_initializer": ("glorot_normal", "glorot_uniform", "zeros"),
                                       "dropout": [i * 0.02 for i in range(11)],
                                       "recurrent_dropout": [i * 0.02 for i in range(11)]}
    
    if not limit_datamodels: limit_datamodels = data_model.available_models
    if not limit_frequencies: limit_frequencies = data_model.available_frequencies
    
    best_params = {}
    for dm in data_model.available_models:
        best_params[dm] = {}
        for freq in data_model.available_frequencies:
            export_fname = Lstm.fname_sideloaded_lstm_autotuning(config = data.config, data_model = dm, frequency = freq)
            hyperparam_scaler = ScaleHyperparameters(model_class = Lstm, data = data, export_yaml_fpath = export_fname, data_model = dm, frequency = freq, scaler = "standard", commodities = cols_contain(data.data["commodity"], "PX_LAST").columns)
            while True:
                try:
                    best_params[dm][freq] = hyperparam_scaler.run(custom_hyperparameters = test_params)
                    break
                except:
                    pass
    
    return best_params

"""
if __name__ == "__main__":
    best_params = autotune_lstm()
"""

#Plot--------------------------------------------------------------------------

def lstm_plotprefix(prefix, data_model, frequency):
    return f"{prefix}_{data_model}_{frequency}"

def plot_process(data, data_model, frequency, commodity):
    lstm = Lstm(data)
    plots = Plots(data.config)
    #frequency = "M" #TODO: Remove me!
    #data_model = "core_model" #TODO: Remove me!
    #commodity = "Copper_PX_LAST" #TODO: Remove me!
    #predictions = lstm.run(frequency, data_model, [commodity], "standard", return_models = False, custom_specification={"epochs": 100}) #TODO: Remove me!
    
    predictions = lstm.run(frequency, data_model, [commodity], "standard", return_models = False) #TODO: Add me!
    
    plots.predictions(predictions,
                      title = f"{translate_resampling(frequency)} Data | {capitalize(data_model).replace('_', ' ')}",
                      fname_prefix = lstm_plotprefix("lstm_logreturns", data_model, frequency),
                      topfolder = data.config["export_folder"]["images"]["lstm"])
    
    plots.model_training(lstm.history,
                         title = f"{commodity} | {translate_resampling(frequency)} | {capitalize(data_model).replace('_', ' ')} | Training history",
                         fname_prefix = lstm_plotprefix(f"lstm_history_{commodity}", data_model, frequency),
                         topfolder = data.config["export_folder"]["images"]["lstm"])

if __name__ == "__main__":
    data = Data()
    data_model_ = DataModel(data)
    plots = Plots(data.config)
    lstm = Lstm(data)
    try: os.makedirs(os.path.join(data.config["export_folder"]["main"], data.config["export_folder"]["images"]["main"], data.config["export_folder"]["images"]["lstm"]))
    except: pass
    procs = []
    for data_model in lstm.data_model.available_models:
        for frequency in lstm.data_model.available_frequencies:
            for commodity in data_model_.core_model(frequency)["y"].columns:
                print(f">>> {commodity} | {data_model} | {frequency}")
                
                p = mp.Process(target = plot_process, args = (data, data_model, frequency, commodity))
                procs.append(p)
                p.start()
                
                #break #TODO: Remove me!
            for p in procs:
                p.join()
            for p in procs:
                p.close()
                procs.remove(p)
            #break #TODO: Remove me!
        #break #TODO: Remove me!