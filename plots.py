#Author------------------------------------------------------------------------

#Benjamin Albrechts


#Import Section----------------------------------------------------------------

from matplotlib import pyplot
import seaborn as sns
import numpy as np
import pandas as pd
import os
from werkzeug.utils import secure_filename

from helpers import load_config, cols_contain, cols_not_contain, mkdir, StandardScaler, capitalize
from data import Data, DataModel


#MAIN--------------------------------------------------------------------------

class Plots:
    
    def __init__(self, config = None):
        self.config = load_config(config)
        self.export_folder = os.path.join(self.config["export_folder"]["main"], self.config["export_folder"]["images"]["main"])
    
    def save_plot(self):
        pass
    
    def data_frame(self, dataframe, title = "", x_label = "", y_label = "", legend = [], legend_loc = 0, subtext = None, fname = "", topfolder = "", new_fig = True):
        if new_fig:
            pyplot.figure()
            pyplot.tight_layout()
        pyplot.plot(dataframe)
        pyplot.xticks(rotation = 25)
        if title: pyplot.title(title)
        if x_label: pyplot.xlabel(x_label)
        if y_label: pyplot.ylabel(y_label)
        if legend: pyplot.legend(legend, loc = legend_loc)
        if subtext: pyplot.figtext(subtext[0], subtext[1], subtext[2])
        if fname:
            if fname.lower() != ".png" and fname.lower() != ".jpg":
                fname = str(fname) + ".png"
            pyplot.savefig(mkdir(os.path.join(self.export_folder, topfolder, secure_filename(fname))), dpi = self.config["plots"]["dpi"], quality = self.config["plots"]["quality"], bbox_inches = "tight")
    
    def predictions(self, container_object, title = "", x_label = "Time", y_label = "", legend_loc = 0, fname_prefix = "", topfolder = "", new_fig = True):
        if not title: title = "Predicted vs. Observed"
        data = container_object.data
        for item in data.keys():
            subtext = (0.45, 0.83, f'R: {round(np.correlate(data[item]["observed"], data[item]["predicted"])[0]*100, 2)}%')
            fname = f"{fname_prefix}_{item}_predicted_vs_actuals.png" if fname_prefix else ""
            title_ = f"{item} | {title}" if title else ""
            self.data_frame(dataframe = data[item], title = title_, x_label = x_label, y_label = y_label, legend = data[item].columns.to_list(), legend_loc = legend_loc, subtext = subtext, fname = fname, topfolder = topfolder, new_fig = new_fig)
    
    def predictions_benchmark(self, container_object, container_object_bm, title = "", x_label = "benchmark", y_label = "model", legend_loc = 0, fname_prefix = "", topfolder = "", new_fig = True):
        if not title: title = "Model vs. Benchmark"
        target = []
        benchmark = []
        legend = []
        for key in container_object.data.keys():
            legend.append(capitalize(key.split("_")[0], ""))
            target.append(np.corrcoef(container_object.data[key]["observed"], container_object.data[key]["predicted"])[0][1]*100)
            benchmark.append(np.corrcoef(container_object_bm.data[key]["observed"], container_object_bm.data[key]["predicted"])[0][1]*100)
        if new_fig:
            pyplot.figure()
            pyplot.tight_layout()
        pyplot.axline([0, 0], [1, 1], color = "grey")
        pyplot.plot(target, benchmark, "o")
        for i, txt in enumerate(legend):
            pyplot.annotate(txt, (target[i], benchmark[i]))
        pyplot.grid()
        if title: pyplot.title(title)
        if x_label: pyplot.xlabel(x_label)
        if y_label: pyplot.ylabel(y_label)
        fname = f"{fname_prefix}_{x_label}_vs_{y_label}.png" if fname_prefix else ""
        if fname:
            if fname.lower() != ".png" and fname.lower() != ".jpg":
                fname = str(fname) + ".png"
            pyplot.savefig(mkdir(os.path.join(self.export_folder, topfolder, secure_filename(fname))), dpi = self.config["plots"]["dpi"], quality = self.config["plots"]["quality"], bbox_inches = "tight")
    
    def model_training(self, tf2_history, title = "", legend_loc = 0, fname_prefix = "", topfolder = "", new_fig = True):
        for commodity in tf2_history.keys():
            history = tf2_history[commodity].history
            dataframe = pd.DataFrame({"TrainSet": history["least_squares"], "DevSet": history["val_least_squares"]})
            dataframe /= dataframe.max()
            self.data_frame(dataframe, title = title, x_label = "Epochs", y_label = "Mean Error", legend = ["Train Set", "Dev Set"], legend_loc = 0, fname = fname_prefix, topfolder = topfolder, new_fig = new_fig)
            
    def correlation_matrix(self, dataframe, title = "Correlation Heatmap", fname = "", topfolder = "", new_fig = True):
        if new_fig:
            pyplot.figure()
            pyplot.tight_layout()
        cmap = sns.diverging_palette(230, 20, as_cmap = True)
        corr = dataframe.corr()
        mask = np.triu(np.ones_like(corr, dtype = bool))
        sns.heatmap(corr, mask = mask, cmap = cmap, annot = round(corr, 2), annot_kws = {"fontsize": 6})
        pyplot.title(title)
        if fname:
            if fname.lower() != ".png" and fname.lower() != ".jpg":
                fname = str(fname) + ".png"
            pyplot.savefig(mkdir(os.path.join(self.export_folder, topfolder, secure_filename(fname))), dpi = self.config["plots"]["dpi"], quality = self.config["plots"]["quality"], bbox_inches = "tight")
    
    def regression_plot(self, dataframe, x_col, y_col, x_label = None, y_label = None, title = "Regression Plot", shift_x = 0, make_stationary = True, fname = "", topfolder = "", new_fig = True):
        if new_fig:
            pyplot.figure()
            pyplot.tight_layout()
        if make_stationary:
            data_frame = np.log(dataframe/dataframe.shift())
        else:
            data_frame = dataframe
        x = data_frame.loc(1)[x_col]
        if shift_x:
            x = x.shift(shift_x)
        sns.regplot(x = x, y = data_frame.loc(1)[y_col], line_kws = {"color": "orange"}).set_title(title)
        if x_label: pyplot.xlabel(x_label)
        if y_label: pyplot.ylabel(y_label)
        if fname:
            if fname.lower() != ".png" and fname.lower() != ".jpg":
                fname = str(fname) + ".png"
            pyplot.savefig(mkdir(os.path.join(self.export_folder, topfolder, secure_filename(fname))), dpi = self.config["plots"]["dpi"], quality = self.config["plots"]["quality"], bbox_inches = "tight")
    
    def px_vol_plot(self, dataframe, price_identifier = ["px_last"], volume_identifier = ["px_volume"], title = "Historical Prices and Volume", legend_loc = 0, subtext = None, fname = "", topfolder = "", new_fig = True):
        prices = cols_contain(dataframe, price_identifier).ffill().dropna()
        prices = prices/prices.max()
        vols = cols_contain(dataframe, volume_identifier).ffill().dropna()
        vols = vols/vols.max()
        vols = vols.join(prices)
        self.data_frame(dataframe = vols, title = title, x_label = "Time", y_label = "Prices", legend = list(vols.columns), legend_loc = legend_loc, subtext = subtext, fname = fname, topfolder = topfolder, new_fig = new_fig)

    def density_plot(self, dataframe, title = "Density", add_random_normal = True, fname = "", topfolder = "", new_fig = True):
        if new_fig:
            pyplot.figure()
            pyplot.tight_layout()
        data_frame = dataframe
        if add_random_normal:
            np.random.seed(0)
            average_mean = np.mean(np.mean(data_frame))
            average_std = np.mean(np.std(data_frame))
            if type(data_frame) == pd.Series:
                data_frame = pd.DataFrame(data_frame)
            data_frame = data_frame.join(pd.Series(np.random.normal(average_mean, average_std, size = len(dataframe.index)), index = data_frame.index, name = "Random Normal Sample"))
        data_frame.plot(title = title, kind = "density")
        if fname:
            if fname.lower() != ".png" and fname.lower() != ".jpg":
                fname = str(fname) + ".png"
            pyplot.savefig(mkdir(os.path.join(self.export_folder, topfolder, secure_filename(fname))), dpi = self.config["plots"]["dpi"], quality = self.config["plots"]["quality"], bbox_inches = "tight")


#TEST--------------------------------------------------------------------------    

if __name__ == "__main__":
    data = Data()
    data_model = DataModel(data)
    plots = Plots(data.config)
    plots.data_frame(StandardScaler(data_model.core_model()["x"]).transform())
    plots.correlation_matrix(data_model.core_model("M")["x"])
    plots.regression_plot(data.data["commodity"], "Gold_PX_VOLUME", "Gold_PX_LAST", shift_x = 1)
    plots.px_vol_plot(cols_contain(data.data["commodity"], "gold"))
    plots.density_plot(cols_not_contain(cols_contain(data.get_logrets()["commodity"], "gold_px_last"), "volume"), title = "Gold")