# Some parts of this code for downloading the data have been copied from here:
# https://stackoverflow.com/questions/38511444/python-download-files-from-google-drive-using-url

import os

if __name__ == "__main__":
    try:
        os.system("pip install -r requirements.txt")
        print(">>> Installation complete <<<")
    except: print("'pip' not installed or not added to path. Please run 'pip install -r requirements.txt' once you have everything set up.")

import requests
import shutil

def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params = { 'id' : id }, stream = True)
    token = get_confirm_token(response)

    if token:
        params = { 'id' : id, 'confirm' : token }
        response = session.get(URL, params = params, stream = True)

    save_response_content(response, destination)    

def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value

    return None

def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)

if __name__ == "__main__":
    from helpers import load_config
    config = load_config()
    file_id = config["data_id"]
    if file_id == "no_key":
        file_id = input("Data password not found. Please contact 'benjamin@albrechts.eu' to gain access.\nIf you already have a key, please paste it here: ")
    destination = 'Data.tar.xz'
    extract_dir = "Data"
    print(">>> Downloading data...")
    download_file_from_google_drive(file_id, destination)
    print(">>> Download finished. Unpacking archive...")
    shutil.unpack_archive(filename = destination)
    print(">>> Unpack complete. Removing archives...")
    os.remove(destination)
    print(">>> Data successfully downloaded and unpacked.")