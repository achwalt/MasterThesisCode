from plots import Plots
from data import Data
from helpers import cols_contain, change_colnames
from linear_models import LinearModels
import pandas as pd
import numpy as np

if __name__ == "__main__":
    plot = Plots()
    data = Data()
    lm = LinearModels()
    
    topfolder = "Supplements"
    
    #ADS---------------------------------------------------------------------------
    
    plot.data_frame(data.data["adsindex"], y_label = "Level", title = "ADS-Index over Time", fname = "ADS_Over_Time", topfolder = topfolder)
    plot.regression_plot((data.data["adsindex"].join(data.data["equity"]["PX_LAST"])).resample("M").last(), x_col = "ADS_Index_050721", y_col = "PX_LAST", x_label = "ADS Index", y_label = "S&P 500", make_stationary = True, fname = "ADS_Regplot", topfolder = topfolder)
    
    #Random Walk-------------------------------------------------------------------
    dataset = cols_contain(data.data["commodity"], "PX_LAST")
    dataset = np.log(dataset/dataset.shift().values)
    models = {}
    for comm in dataset.columns:
        
        y, x = cols_contain(dataset, comm), cols_contain(dataset, comm)
        
        for i in range(1, 10):
            x = pd.DataFrame(x).join(change_colnames(cols_contain(dataset.shift(i), comm), [f"t-{i}"]))
        
        x, y = cols_contain(x, "t-"), y
        x, y = data.consistent_indices(x, y)
        x, y = x.dropna(), y.dropna()
    
    
        models[comm] = lm.ols(x, y, intercept = False, cov_type = "hc1")[comm]
        print(models[comm].summary())
        #lm.export_regressions(models=models, fname = "RandomWalk")
    lm.export_regressions(models, fname = "random_walk_hypothesis", caption = "Random walk? OLS regression of lagged returns.")
